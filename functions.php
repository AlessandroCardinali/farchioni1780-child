<?php

/**
 * Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 */

// Hard-coded options
define( 'FARCHIONI_LABEL_OFFERTE', 'Promo' );
define( 'FARCHIONI_SOGLIA_SCONTO_PER_OFFERTE', 8.9 );
define( 'FARCHIONI_REWRITE_PRODUCT_SLUG', true );


/**
 * Meta usati da ACF per storare gli indicatori
 */
define(
	'FARCHIONI_INDICATORI_TRANSLATIONS_META',
	[
		'indicatori_select_olio' => 'indicatori_select_olio_${i}_nome_indicatore',
		'indicatori_select'      => 'indicatori_select_${i}_nome_indicatore',
	]
);

/**
 * Traduzioni degli indicatori in inglese
 */
define(
	'FARCHIONI_INDICATORI_TRANSLATIONS_VALUES',
	[
		'Dolce'       => 'Sweet',
		'Amaro'       => 'Bitter',
		'Fruttato'    => 'Fruity',
		'Intensità'   => 'Intensity',
		'Piccante'    => 'Spicy',
		'Struttura'   => 'Structure',
		'Persistenza' => 'Persistence',
		'Aromi'       => 'Aromas',
	]
);


/**
 * This is the array that will be serialized inside `wcb2b_product_group_prices` meta for each product
 * It is used to store the price of the b2b aka Professionals
 * Please update the ids to be the same as the wcb2b groups
 */
define( 'FARCHIONI_USER_GROUP_B2C', 118905 );
define( 'FARCHIONI_USER_GROUP_B2B', 118906 );
define( 'FARCHIONI_USER_GROUP_B2B_PENDING', 118907 );

// Load Composer
require_once 'vendor/autoload.php';

// Theme Setup
require_once 'includes/help-functions.php';
require_once 'includes/theme-setup.php';
require_once 'includes/sib-newsletter.php';

// CLI tools
require_once 'includes/cli.php';

// WooCommerce
require_once 'includes/woocommerce/archive.php';
require_once 'includes/woocommerce/product.php';
if ( function_exists( 'ntav_netreviews_parse_request' ) ) {
	require_once 'includes/woocommerce/netreviews.php';
}
require_once 'includes/woocommerce/legacy.php';
require_once 'includes/woocommerce/mini-cart.php';
require_once 'includes/woocommerce/checkout.php'; // Require composer package idearia/vies
require_once 'includes/woocommerce/shortcode-product.php';
require_once 'includes/woocommerce/csv-importer.php';
require_once 'includes/woocommerce/order.php';

// WooCommerce B2C
require_once 'includes/woocommerce/registration_b2c.php';

// Woocommerce B2B
require_once 'includes/woocommerce-b2b/init-notifications.php';
require_once 'includes/woocommerce/registration_b2b.php';
require_once 'includes/woocommerce-b2b/wcb2b-cross-sell.php';
require_once 'includes/woocommerce-b2b/csv-importer.php';
require_once 'includes/woocommerce-b2b/tax-price.php';

// Libori Functions
require_once 'libori.php';



/* ======================================================
	* Fosforica
	* Funzione passaggio email da form a Pagina Newsletter
====================================================== */
add_action( 'wp_footer', 'fosforica_page_newsletter' );
function fosforica_page_newsletter() {
	if( is_page( array(126878,126896)) ) { //pagina form - ita 126878 -  eng 126896
	?>
		<script type="text/javascript" id="passedEmail">
			const queryString = window.location.search; // passo l'input mail alla pagina
			const urlParams = new URLSearchParams(queryString);
			const sendEmail = urlParams.get('sendEmail'); //console.log(sendEmail);
			if (sendEmail) {
				document.getElementById("passedEmail").value = sendEmail;
				window.history.replaceState(null, null, window.location.pathname); // pulisce la query string
			}
		</script>
	<?php }
}

/* ======================================================
	* Fosforica
	* Single product Premi prodotto sotto add to cart
====================================================== */
add_action( 'woocommerce_share' , 'fosfo_single_premi', 15 );
function fosfo_single_premi() {
	if ( have_rows( 'premi_prodotto' ) ) { ?>
<style id="fos-tooltip">.tooltip{position:relative;display:inline-block}.tooltip .tooltiptext{visibility:hidden;width:240px;background-color:#555;color:#fff;text-align:center;padding:6px;border-radius:6px;font-size:14px;position:absolute;z-index:1;bottom:125%;left:50%;margin-left:-120px;opacity:0;transition:opacity .3s}.tooltip .tooltiptext::after{content:"";position:absolute;top:100%;left:50%;margin-left:-5px;border-width:5px;border-style:solid;border-color:#555 transparent transparent transparent}.tooltip:hover .tooltiptext{visibility:visible;opacity:1}</style>
		<div class="row justify-content-center align-items-md-center mt-4 mt-xl-5">
		<?php if ( have_rows( 'premi_prodotto' ) ) {
			while ( have_rows( 'premi_prodotto' ) ) { the_row(); ?>
			<div class="col-6 col-sm-3 col-xl-2 mb-3 mb-sm-0 px-1 px-lg-3">
				<div class="tooltip">
					<img class="img-fluid w-100 mb-2 px-5 px-sm-0" src="<?php the_sub_field( 'immagine_premio' ); ?>" />
					<span class="tooltiptext d-none d-md-block"><?php the_sub_field( 'testo_premio' ); ?></span>
				</div>
				<div style="font-size: 12px; text-align: center; color: #a7a7a7;" class="d-block d-md-none"><?php the_sub_field( 'testo_premio' ); ?></div>
			</div>
		<?php }	} ?>
	</div>
<?php	}
}



/* ======================================================
	* Fosforica
	* Add a message above the login / register form on my-account page
====================================================== */
add_action( 'woocommerce_before_customer_login_form', 'jk_login_message' );
function jk_login_message() {
    if ( get_option( 'woocommerce_enable_myaccount_registration' ) == 'yes' ) {
	    echo '<div class="mb-4 d-flex justify-content-center">';
	    echo do_shortcode('[nextend_social_login]');
	    echo '</div>';
	}
}



/* ======================================================
	* Fosforica
	* BOLLINI LOOP PRODOTTI
====================================================== */
add_action('woocommerce_before_shop_loop_item', 'fosfo_custm_badges', 10);
add_action('woocommerce_before_single_product_summary', 'fosfo_custm_badges', 10);
function fosfo_custm_badges() {
	$bollini = get_field('bollino_prodotto');
	if( $bollini ):
	echo '<div class="container-bollini">';
		foreach( $bollini as $bollo ):
	    $label = $bollo['label']; // titolo
			$value = $bollo['value']; // valore per nome immagine
			$dir = get_stylesheet_directory_uri().'/bollini-adv';
			$media = $dir.'/'.$value.'.png';
			?>
			<img src="<?php echo $media; ?>" class="img-fluid d-block bollino <?php echo $value; ?>" width="60">
			<?php
			//echo $media.' '.$label.' '.$value;
    endforeach;
   echo '</div>';
	 endif;
}
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 25 );



/* ======================================================
	 * Tosi - Escludi Corrieri per fasce di peso
 ====================================================== */
 // REF: https://www.businessbloomer.com/woocommerce-shipping-weight-without-plugin/
 // REF: https://stackoverflow.com/questions/63685736/require-minimum-weight-for-delivery-method-in-woocommerce

 function filter_woocommerce_package_rates( $rates, $package ) {
	 // Get cart contents weight
	 $weight = WC()->cart->get_cart_contents_weight();

	// escludi mbe se superiore a 20 kg
	if ($weight > 20 ) {
		foreach ( $rates as $rate_key => $rate ) {
			if ( strpos($rate->method_id, 'wf_mbe_shipping') !== false ){
				unset( $rates[$rate_key] );
			}
		}
	}

	// escludi ups se minore di 20 kg o superiore a 50 kg
	if ($weight <= 20 || $weight > 50 ) {
		foreach ( $rates as $rate_key => $rate ) {
			if ( strpos($rate->method_id, 'ups') !== false ){
				unset( $rates[$rate_key] );
			}
		}
	}

	return $rates;
 }
 add_filter( 'woocommerce_package_rates', 'filter_woocommerce_package_rates', 10, 2 );


 /* ======================================================
	* Tosi - se il plugin Fosforica viene sganciato evita errore 500
	 ====================================================== */
	/**
	 if ( !function_exists( 'is_b2b_user' ) ){
	 // CHECK se utente loggato come B2B o meno
	 function is_b2b_user(){
 $is_b2b = (is_user_logged_in() && get_user_meta( get_current_user_id(), 'wcb2b_group', true) == '118906')? true : false;
 return $is_b2b;
	 }
	 }
	 **/
