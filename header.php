<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="//gmpg.org/xfn/11">
        <?php wp_head(); ?>
    </head>
<body <?php body_class(); ?>>
<?php
wp_body_open();
// Use Elementor footer instead of the default one
// If Elementor is installed, use its header instead of the default one
$isElementorActive = class_exists( 'ElementorPro\Modules\ThemeBuilder\Classes\Theme_Support' );
if ( $isElementorActive ) {
	/**
	--> Nome Template: Main Header with Sticky
	Per modificare la nav b2b
	1. aprire la modifica di elementor dalla home page
	2. Modificare la testata
	3. utilizzare il navigatore per modificare le parti nascoste della nav b2b
	**/
	//$elementorThemeSupport = new ElementorPro\Modules\ThemeBuilder\Classes\Theme_Support();
	//$elementorThemeSupport->get_header( '118192' );
	echo do_shortcode( '[elementor-template id="118192"]');
} else { ?>
<header id="site-header" class="site-header base-site-header">
	<div class="container">
		<div class="wrap-header">
			<div class="row">
				<div id="site-identity" class="col-8 col-md-3 site-identity">
					<?php get_template_part('inc/templates/logo'); ?>
				</div>
				<div class="col-4 col-md-9 wrap-site-navigation">
					<nav id="primary-menu" class="primary-menu">
						<span class="button-close-nav close-nav">
							<i class="zoo-icon-close"></i>
						</span>
						<?php
							// Aggiornare le url tramite il menu wp cliccando su JetMenu (Elementor di merda)
						wp_nav_menu(array(
							'theme_location' => 'primary-menu',
							'container' => false,
							'container_id' => false,
							'container_class' => false,
							'menu_id' => false,
							'menu_class' => 'menu nav-menu',
							'walker' => zoo_get_walker_nav_menu(),
							'fallback_cb' => false,
						));
						?>
					</nav>
					<div class="menu-overlay close-nav"></div>
					<span class="nav-button">
						<i class="zoo-css-icon-menu"></i>
					</span>
				</div>
			</div>
		</div>
	</div>
</header>
<?php }
