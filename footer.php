<?php
// Use Elementor footer instead of the default one
// If Elementor is installed, use its header instead of the default one
$isElementorActive = class_exists( 'ElementorPro\Modules\ThemeBuilder\Classes\Theme_Support' );
if ( $isElementorActive ) {
	//echo do_shortcode( '[elementor-template id="118203"]' );
	//$elementorThemeSupport = new ElementorPro\Modules\ThemeBuilder\Classes\Theme_Support();
	//$elementorThemeSupport->get_footer( '118203' );
	echo do_shortcode( '[elementor-template id="118203"]');
} else { ?>
<div class="zoo-mask-close"></div>
<footer id="site-footer" class="site-footer">
    <div class="container">
        <?php
        echo esc_html(get_theme_mod('zoo_footer_copy_right',sprintf(esc_html__( '© %s ZooTemplate. All rights reserved.', 'doma' ),date("Y"))));
        ?>
    </div>
</footer>
<?php
}
wp_footer();
?>

</body>
</html>
