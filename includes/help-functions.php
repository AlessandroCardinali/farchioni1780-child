<?php


/**
 * Remove filter/action without the method name
 * https://wordpress.stackexchange.com/questions/304859/remove-action-from-a-plugin-class
 */
function remove_filters_with_method_name( $hook_name = '', $method_name = '', $priority = 0 ) {
	global $wp_filter;
	// Take only filters on right hook name and priority
	if ( ! isset( $wp_filter[ $hook_name ][ $priority ] ) || ! is_array( $wp_filter[ $hook_name ][ $priority ] ) ) {
		return false;
	}
	// Loop on filters registered
	foreach ( (array) $wp_filter[ $hook_name ][ $priority ] as $unique_id => $filter_array ) {
		// Test if filter is an array ! (always for class/method)
		if ( isset( $filter_array['function'] ) && is_array( $filter_array['function'] ) ) {
			// Test if object is a class and method is equal to param !
			if ( is_object( $filter_array['function'][0] ) && get_class( $filter_array['function'][0] ) && $filter_array['function'][1] == $method_name ) {
				// Test for WordPress >= 4.7 WP_Hook class (https://make.wordpress.org/core/2016/09/08/wp_hook-next-generation-actions-and-filters/)
				if ( is_a( $wp_filter[ $hook_name ], 'WP_Hook' ) ) {
					unset( $wp_filter[ $hook_name ]->callbacks[ $priority ][ $unique_id ] );
				} else {
					unset( $wp_filter[ $hook_name ][ $priority ][ $unique_id ] );
				}
			}
		}
	}
	return false;
}

/**
 * Check if the product should be on sale or not
 * @param WC_Product $product
 * @param float      $isDiscountAbove (percentage) The product is on sale only if it has one of it's variations discount percentage aboute this value
 */
function farchioni_product_is_on_sale( WC_Product $product, float $isDiscountAbove ) : bool {
	if ( $product->is_type( 'grouped' ) ) {
		//TODO: caso non considerato
		return false;
	}

	if ( ! $product->has_child() ) {
		//TODO: caso non considerato
		return false;
	}

	$variationsId = $product->get_visible_children(); // Get product child id
	$percentages  = []; //phpcs:ignore

	// check the biggest sale available
	foreach ( $variationsId as $variationId ) {
		$productVariation = wc_get_product( $variationId );
		// skip if product is not on sale
		if ( ! $productVariation->is_on_sale() ) {
			continue;
		}
		$percentages[] = round( ( $productVariation->get_regular_price() - $productVariation->get_sale_price() ) / $productVariation->get_regular_price() * 100 );
	}

	// Check if the percentage is less or equal to $isDiscountAbove
	if ( empty( $percentages ) || max( $percentages ) <= $isDiscountAbove ) {
		return false;
	}

	return true;
}
