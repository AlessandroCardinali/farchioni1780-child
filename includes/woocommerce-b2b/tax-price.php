<?php

add_filter( 'option_woocommerce_price_display_suffix', 'farchioni_label_price_vat_excluded' );


/**
 * Show tax suffix on single product page
 */
function farchioni_label_price_vat_excluded($suffix) {
    
    if (!is_product()) {
        return $suffix;
    }

    if (get_option('wcb2b_split_taxes') !== 'yes') {
        return $suffix;
    }

    $current_status = get_the_author_meta( 'wcb2b_status', get_current_user_id() );
    $current_group = get_the_author_meta( 'wcb2b_group', get_current_user_id() );

    if (FARCHIONI_USER_GROUP_B2B != $current_group) {
        return $suffix;
    }

    return __('Taxes excluded', 'farchioni1780-child');
}