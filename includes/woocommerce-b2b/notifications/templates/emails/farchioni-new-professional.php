<?php
/**
 * Customer on quote notification
 *
 * @version 3.0.0
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_email_header', $email_heading, $email );

?>
<p>
    <?php esc_html_e( 'A new user want to become Professional', 'farchioni1780-child' ); ?>
</p>

<?php do_action( 'woocommerce_email_footer', $email ); ?>