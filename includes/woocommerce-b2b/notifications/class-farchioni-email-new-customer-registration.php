<?php

defined( 'ABSPATH' ) || exit;

if ( class_exists( 'Farchioni_Email_New_Customer_Registration', false ) ) {
	return new Farchioni_Email_New_Customer_Registration();
}

/**
 * WooCommerce B2B Email new customer registration
 *
 * @version 3.0.0
 */
class Farchioni_Email_New_Customer_Registration extends WC_Email {
	//phpcs:ignore Squiz.Commenting.VariableComment.Missing
	public $recipients = array();

	/**
	 * Constructor
	 */
	public function __construct() {
		error_log('calling Farchioni_Email_New_Customer_Registration::class');
		$this->id          = 'farchioni_new_professional';
		$this->title       = __( 'New Professional account', 'farchioni1780-child' );
		$this->description = __( 'Send email notification to admin when a new professional account has been created.', 'farchioni1780-child' );

		$this->template_html    = '/templates/emails/farchioni-new-professional.php';
		$this->template_plain    = '/templates/emails/farchioni-new-professional.php';
		// $this->template_plain   = 'emails/plain/farchioni-new-professional.php';
		// do_action( 'woocommerce_created_customer', $customer_id, $new_customer_data, $password_generated );
		add_action( 'edit_user_profile_update', array( $this, 'trigger' ), 150, 1 );

		parent::__construct();

		$this->template_base = dirname( __FILE__ );

		$this->recipient = $this->get_option( 'recipient', get_option( 'admin_email' ) );
	}

	/**
	 * Get email subject
	 *
	 * @return string
	 */
	public function get_default_subject() {
		return __( 'Un nuovo utente desidera diventare Professional!', 'farchioni1780-child' );
	}

	/**
	 * Get email heading
	 *
	 * @return string
	 */
	public function get_default_heading() {
		return __( 'Nuova richiesta di account Professional.', 'farchioni1780-child' );
	}

	/**
	 * Trigger the sending of this email
	 */
	public function trigger( $user_id ) {

		/**
		 * TODO: Do the magic anche check if we can send the email!
		 */

		// if ( get_user_meta( $customer_id, 'wcb2b_group', 'true' ) !== '18900' ) {
		// 	return false;
		// }

		$current_status = get_the_author_meta( 'wcb2b_status', $user_id );
		$current_group = get_the_author_meta( 'wcb2b_group', $user_id );
		
		if ( FARCHIONI_USER_GROUP_B2B_PENDING === $current_group && '-1' === $current_status ){

			error_log( '>>>>>>>>>>>>>>>Trigger sending email:' . var_export( $user_id, true ) );
			error_log( '>>>>>>>>>>>>>>>Trigger sending email:' . var_export( $this->is_enabled(), true ) );
			error_log( '>>>>>>>>>>>>>>>Trigger sending email:' . var_export( $this->get_recipient(), true ) );

			if ( $this->is_enabled() && $this->get_recipient() ) {
				$this->send( $this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments() );
			}

		}

		

		

	}

	/**
	 * Get content html
	 *
	 * @return string
	 */
	public function get_content_html() {
		return wc_get_template_html(
			$this->template_html,
			[
				'order'              => $this->object,
				'email_heading'      => $this->get_heading(),
				'additional_content' => $this->get_additional_content(),
				'blogname'           => $this->get_blogname(),
				'sent_to_admin'      => true,
				'plain_text'         => false,
				'email'              => $this,
			],
			'',
			$this->template_base
		);
	}

	/**
	 * Get content plain
	 *
	 * @return string
	 */
	public function get_content_plain() {
		return wc_get_template_html(
			$this->template_plain,
			[
				'order'              => $this->object,
				'email_heading'      => $this->get_heading(),
				'additional_content' => $this->get_additional_content(),
				'blogname'           => $this->get_blogname(),
				'sent_to_admin'      => true,
				'plain_text'         => true,
				'email'              => $this,
			],
			'',
			$this->template_base
		);
	}

}
