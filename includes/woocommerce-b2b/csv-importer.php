<?php

add_action( 'woocommerce_product_import_pre_insert_product_object', 'farchioni_add_b2b_price', 10, 2 );

define(
	'FARCHIONI_B2B_SINGLE_PRODUCT_PRICE',
	[
		FARCHIONI_USER_GROUP_B2C => [
			'regular_price' => '',
			'sale_price'    => '',
		], // b2c
		FARCHIONI_USER_GROUP_B2B => [
			'regular_price' => '',
			'sale_price'    => '',
		], // b2b
		FARCHIONI_USER_GROUP_B2B_PENDING => [
			'regular_price' => '',
			'sale_price'    => '',
		], // pending
	]
);

/**
 * Imposta il prezzo del b2b in un campo serializato dentro il meta `wcb2b_product_group_prices`
 */
function farchioni_add_b2b_price( $product, $data ) {

	// Get product description
	$b2bMetaPrice = $product->get_meta( '_prezzo_b2b_temp', true );
	$b2bMetaDiscountPrice = $product->get_meta( '_prezzo_b2b_scontato_temp', true );

	$b2bPrice = FARCHIONI_B2B_SINGLE_PRODUCT_PRICE;
	if ( $b2bMetaPrice !== false ) {
		$b2bPrice[ FARCHIONI_USER_GROUP_B2B ]['regular_price'] = $b2bMetaPrice;
		$b2bPrice[ FARCHIONI_USER_GROUP_B2B ]['regular_price'] = str_replace( ',', '.', $b2bPrice[ FARCHIONI_USER_GROUP_B2B ]['regular_price'] );
	}

	if ( $b2bMetaDiscountPrice !== false ) {
		$b2bPrice[ FARCHIONI_USER_GROUP_B2B ]['sale_price'] = $b2bMetaDiscountPrice ?? '';
		$b2bPrice[ FARCHIONI_USER_GROUP_B2B ]['sale_price'] = str_replace( ',', '.', $b2bPrice[ FARCHIONI_USER_GROUP_B2B ]['sale_price'] );
	}

	if ( $b2bMetaPrice !== false ) {
		$product->add_meta_data( 'wcb2b_product_group_prices', $b2bPrice, true );
	}
	return $product;
}
