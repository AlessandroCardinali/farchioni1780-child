<?php

/**
 * Add transactional email for quotations
 *
 * @param array $emails Emails settings
 * @return array
 */
function set_quotations_emails( $emails ) {
	require_once dirname( __FILE__ ) . '/notifications/class-farchioni-email-new-customer-registration.php';
	$emails['WC_Check_Availability_Email'] = new Farchioni_Email_New_Customer_Registration();
	return $emails;
}

// add_filter( 'woocommerce_email_classes', 'set_quotations_emails' );
