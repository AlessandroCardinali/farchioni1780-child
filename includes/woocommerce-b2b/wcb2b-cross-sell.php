<?php

/**
 * The plugin is enabling cross-sell on single and cart page
 * https://idearia.link/tGd7Fq
 */

remove_filters_with_method_name( 'woocommerce_cart_collaterals', 'hide_in_cart', 10 );
remove_filters_with_method_name( 'woocommerce_after_single_product_summary', 'hide_in_single', 15 );
