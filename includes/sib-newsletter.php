<?php

add_shortcode( 'print_sib_newsletter', 'print_sib_newsletter_language_based' );


/**
 * Print Sendinblue newsletter form based on language
 */
function print_sib_newsletter_language_based( $atts ) {

    $params = shortcode_atts( array(
		'it_form_id' => '11',
		'en_form_id' => '12',
    ), $atts );

    $currentLanguage = null;
    if ( defined( 'ICL_LANGUAGE_CODE' ) ) {
        $currentLanguage = ICL_LANGUAGE_CODE;
    }

    if ($currentLanguage == 'it') {
        return do_shortcode('[sibwp_form id=' . $params['it_form_id'] . ']');
    }

    return do_shortcode('[sibwp_form id=' . $params['en_form_id'] . ']');
}