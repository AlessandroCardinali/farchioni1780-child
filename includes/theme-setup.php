<?php

/**
 * HOOKS
 */

add_action( 'wp_enqueue_scripts', 'farchioni_child_theme_enqueue_styles' );
add_action( 'wp_enqueue_scripts', 'farchioni_enqueue_font_awesome' );
add_action( 'wp_print_styles', 'farchioni_dequeue_unnecessary_styles' );
add_action( 'wp_print_scripts', 'farchioni_dequeue_unnecessary_scripts' );
add_action( 'after_setup_theme', 'farchioni1780_setup' );
add_filter( 'body_class', 'farchioni_language_class' );

add_filter('woocommerce_show_variation_price', function() { return TRUE;});

function farchioni1780_setup() {
	load_child_theme_textdomain( 'farchioni1780-child', get_stylesheet_directory() . '/languages' );
}

/**
 *  Current WPML lang as body class
 */
function farchioni_language_class( $classes ) {
	$classes[] = 'current_lang_' . ICL_LANGUAGE_CODE;
	return $classes;
}

// TODO: filter to use this only on frontend
// add_filter( 'script_loader_tag', 'farchioni_add_style_attributes', 10, 2 );

/**
 * FUNCTIONS
 */

/**
 * Register theme script and style
 */
function farchioni_child_theme_enqueue_styles() {
	// commentato da Fosforica
	// wp_enqueue_style( 'farchioni1780-fonts', get_stylesheet_directory_uri() . '/assets/fonts/style.css', array(), filemtime( get_stylesheet_directory() . '/assets/fonts/style.css' ) );
	wp_enqueue_style( 'farchioni1780-style', get_stylesheet_directory_uri() . '/assets/css/style.min.css', array(), filemtime( get_stylesheet_directory() . '/assets/css/style.min.css' ) );
	wp_enqueue_script( 'farchioni1780-app', get_stylesheet_directory_uri() . '/assets/js/custom.min.js', array(), filemtime( get_stylesheet_directory() . '/assets/js/custom.min.js' ), true );

	// commentato da fosforica per caricare versione script non minificata
	//wp_enqueue_script( 'farchioni1780-vendor', get_stylesheet_directory_uri() . '/assets/js/vendor.min.js', array(), filemtime( get_stylesheet_directory() . '/assets/js/vendor.min.js' ), true );
	wp_enqueue_script( 'farchioni1780-vendor', get_stylesheet_directory_uri() . '/assets/js/vendor.js', array(), filemtime( get_stylesheet_directory() . '/assets/js/vendor.js' ), true );


	// aggiunto da Fosforica
	wp_enqueue_style( 'farchioni1780-fix', get_stylesheet_directory_uri() . '/assets/css/fix.css', array(), filemtime( get_stylesheet_directory() . '/assets/css/fix.css' ) );

}

/**
 * Register Font Awesome CDN
 */
function farchioni_enqueue_font_awesome() {
	//phpcs:ignore WordPress.WP.EnqueuedResourceParameters.MissingVersion
	wp_register_script( 'font-awesome-5', 'https://kit.fontawesome.com/9ea874df3b.js', null, null, true );
	wp_enqueue_script( 'font-awesome-5' );
}

/**
 * Add inline styles attribute
 */
function farchioni_add_style_attributes( $html, $handle ) {
	if ( 'font-awesome-5' === $handle ) {
		return str_replace( ".js'", ".js' crossorigin='anonymous'", $html );
	}
}

/**
 * Dequeue Styles
 */
function farchioni_dequeue_unnecessary_styles() {
	// Remove standard css from parent theme, the parts we want to keep are inside sass/vendors/_doma_*.scss
	wp_dequeue_style( 'zoo-styles' );
	wp_deregister_style( 'zoo-styles' );

	wp_dequeue_style( 'zoo-custom-styles' );
	wp_deregister_style( 'zoo-custom-styles' );

	// wp_dequeue_style( 'clever-font' ); - Teniamo ancora la font che è usata anche dentro il megamenu
	// wp_deregister_style( 'clever-font' );
}

/**
 * Dequeue JavaScripts
 */
function farchioni_dequeue_unnecessary_scripts() {
	wp_dequeue_script( 'zoo-woo-ajax' );
	wp_deregister_script( 'zoo-woo-ajax' );

	/**
	 * Script inserito nella cartella vendor
	 *  - eliminata la parte che generava errore su "Cannot read property 'top' of undefined"
	 *  - Spostato nel file woocommerce-cart.js la parte di applicazione coupon nel carrello.
	 */
	wp_dequeue_script( 'zoo-woocommerce' );
	wp_deregister_script( 'zoo-woocommerce' );

	wp_dequeue_script( 'zoo-mega-menu' );
	wp_deregister_script( 'zoo-mega-menu' );

	wp_dequeue_script( 'zoo-wishlist' );
	wp_deregister_script( 'zoo-wishlist' );

	wp_dequeue_script( 'zoo-products-compare' );
	wp_deregister_script( 'zoo-products-compare' );

	wp_dequeue_script( 'zoo-scripts' );
	wp_deregister_script( 'zoo-scripts' );

	wp_dequeue_script( 'zoo-theme-builder-elements' );
	wp_deregister_script( 'zoo-theme-builder-elements' );

	wp_dequeue_script( 'defer-js' );
	wp_deregister_script( 'defer-js' );

	wp_dequeue_script( 'countdown' );
	wp_deregister_script( 'countdown' );

	wp_dequeue_script( 'slick' );
	wp_deregister_script( 'slick' );

	wp_dequeue_script( 'sticky-kit' );
	wp_deregister_script( 'sticky-kit' );
}
