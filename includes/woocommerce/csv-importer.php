<?php
// Better Markdown Parser in PHP for description and short description
add_action( 'woocommerce_product_import_pre_insert_product_object', 'farchioni_format_product_drescription_meta', 10, 2 );

// Convert pdf url to pdf id
add_action( 'woocommerce_product_import_pre_insert_product_object', 'farchioni_format_product_pdf_etichetta_meta', 10, 2 );

// Convert immagine descrizione from url to id
add_action( 'woocommerce_product_import_pre_insert_product_object', 'farchioni_format_product_immagine_desc_meta', 10, 2 );

// Convert immagine premi from url to id
add_action( 'woocommerce_product_import_pre_insert_product_object', 'farchioni_format_product_premi_prodotto_meta', 10, 2 );

// Imposta campi da tradurre in fase di import
add_filter( 'woocommerce_csv_import_wpml_support/filter_columns', 'farchioni_wpml_csv_import_columns', 10, 1 );

// Traduci i campi Indicatori in fase di import
add_action( 'woocommerce_csv_import_wpml_support/process_translation', 'farchioni_translate_meta_indicatori', 50, 1 );

// Mantieni gli slug uguali a quelli live
if ( FARCHIONI_REWRITE_PRODUCT_SLUG ) {
	add_action( 'woocommerce_product_import_pre_insert_product_object', 'farchioni_update_product_slug', 15, 2 );
	add_action( 'woocommerce_csv_import_wpml_support/process_translation', 'farchioni_translate_product_slug', 60, 1 );
}


/**
 * Trasforma il campo della descrizione in html markdown
 */
function farchioni_format_product_drescription_meta( WC_Product $product, $data ) {

	$parsedown = new Parsedown();

	// get the product description
	$description = $product->get_meta( 'campo_descrizione_lunga', true );

	// parse the description
	$description = $parsedown->text( $description );

	$product->add_meta_data( 'campo_descrizione_lunga', $description, true );

	return $product;
}

/**
 * Trasforma il campo del etichetta da titolo del media a id
 */
function farchioni_format_product_pdf_etichetta_meta( WC_Product $product, $data ) {

	$pdfTitle = $product->get_meta( 'pdf_etichetta', true );

	// Non fare niente se l'utente ha passato un valore vuoto
	if ( ! $pdfTitle ) {
		return $product;
	}

	// Cerca il PDF e impostalo nel meta dell'etichetta
	$pdfId = wordpress_get_attachment_id_by_filename( $pdfTitle );
	if ( $pdfId && is_numeric( $pdfId ) ) {
		$product->add_meta_data( 'pdf_etichetta', $pdfId, true );
	}

	return $product;
}

/**
 * Trasforma il campo del immagine descrizione da url a id
 */
function farchioni_format_product_immagine_desc_meta( WC_Product $product, $data ) {

	// get the product description
	$imgUrl = $product->get_meta( 'immagine_desc', true );
	// https://developer.wordpress.org/reference/functions/attachment_url_to_postid/
	$imgId = wordpress_get_attachment_id_by_filename( $imgUrl );

	if ( $imgId !== false ) {
		$product->add_meta_data( 'immagine_desc', $imgId, true );
	}

	return $product;
}

/**
 * Trasforma il campo del immagine dei premi da url a id
 */
function farchioni_format_product_premi_prodotto_meta( WC_Product $product, $data ) {

	// get the product description
	$totalFields = $product->get_meta( 'premi_prodotto', true );

	for ( $i = 0; $i < $totalFields; $i++ ) {
		// get the product description
		$imgUrl = $product->get_meta( 'premi_prodotto_' . $i . '_immagine_premio', true );
		$imgId = wordpress_get_attachment_id_by_filename( $imgUrl );
		if ( $imgId !== false ) {
			$product->add_meta_data( 'premi_prodotto_' . $i . '_immagine_premio', $imgId, true );
		}
	}

	return $product;
}


/**
 * Find attachment ID by its file name.
 *
 * Source: https://wordpress.stackexchange.com/a/117087/86662
 */
function wordpress_get_attachment_id_by_filename( $file_name, $post_status = array( 'public', 'draft', 'inherit' ) ) {

	$attachment = wordpress_get_attachment_by_filename( $file_name, $post_status );

	if ( isset( $attachment->ID ) ) {
		return $attachment->ID;
	} else {
		return false;
	}
}


/**
 * Find attachment by file name
 *
 * Source: https://wordpress.stackexchange.com/a/117087/86662
 */
function wordpress_get_attachment_by_filename( $file_name, $post_status = array( 'public', 'draft', 'inherit' ) ) {

	if ( empty( $file_name ) ) {
		return false;
	}

	global $wpdb;
	// phpcs:ignore WordPress.DB.PreparedSQL.NotPrepared, WordPress.DB.DirectDatabaseQuery.DirectQuery
	$post = $wpdb->get_results( 'SELECT wp_posts.* FROM wp_posts, wp_postmeta WHERE wp_postmeta.meta_key = "_wp_attached_file" AND wp_postmeta.meta_value LIKE "%' . $file_name . '" AND wp_postmeta.post_id = wp_posts.ID ORDER by wp_posts.post_modified DESC LIMIT 1;' );

	if ( empty( $post ) || ! isset( $post[0] ) ) {
		return false;
	}

	return $post[0];
}

/**
 * Colonne da tradurre con WPML ad ogni import
 * di prodotto da CSV
 */
function farchioni_wpml_csv_import_columns( $default_columns ) {
	return [
		[
			'field' => 'Name',
			'type'  => 'product_name',
		],
		[
			'field' => 'Descrizione breve',
			'type'  => 'product_short_description',
		],
		[
			'field'    => 'Descrizione lunga',
			'type'     => 'product_meta',
			'meta_key' => 'campo_descrizione_lunga',
			'markdown' => true,
		],
		[
			'field' => 'Description',
			'type'  => 'product_description',
		],
		[
			'field'    => 'Sottotitolo',
			'type'     => 'product_meta',
			'meta_key' => 'sottotitolo',
		],
		// [
		// 	'field'    => 'SEO - Title',
		// 	'type'     => 'product_meta',
		// 	'meta_key' => '_yoast_wpseo_title',
		// ],
		// [
		// 	'field'    => 'SEO - Meta Description',
		// 	'type'     => 'product_meta',
		// 	'meta_key' => '_yoast_wpseo_metadesc',
		// ],
		[
			'type'     => 'product_meta_copy',
			'meta_key' => '_premi_prodotto',
		],
		[
			'type'     => 'product_meta_copy',
			'meta_key' => '_indicatori_select',
		],
		[
			'type'     => 'product_meta_copy',
			'meta_key' => '_indicatori_select_olio',
		],
		[
			'type'     => 'product_meta_copy',
			'meta_key' => '_woocommerce_gpf_data',
		],
	];
}

/**
 * Traduci i meta indicatori  del prodotto
 */
function farchioni_translate_meta_indicatori( $translatedProduct ) {

	$product = wc_get_product( $translatedProduct->get_id() );

	if ( ! $product instanceof WC_Product ) {
		return;
	}

	// configura il parametro FARCHIONI_INDICATORI_TRANSLATIONS_META in functions.php
	foreach ( FARCHIONI_INDICATORI_TRANSLATIONS_META as $metaKey => $metaValue ) {

		$indicatore = $product->get_meta( $metaKey );

		if ( ! is_numeric( $indicatore ) ) {
			continue;
		}

		for ( $i = 0; $i < $indicatore; $i++ ) {
			$metaToTranslate = str_replace( '${i}', $i, $metaValue );
			$nomeIndicatore = $product->get_meta( $metaToTranslate );

			// configura il parametro FARCHIONI_INDICATORI_TRANSLATIONS_VALUES in functions.php
			if ( ! isset( FARCHIONI_INDICATORI_TRANSLATIONS_VALUES[ $nomeIndicatore ] ) ) {
				error_log( __FILE__ . ':' . __FUNCTION__ . ':' . __LINE__ . ': Non ho trovato la traduzione in FARCHIONI_INDICATORI_TRANSLATIONS_VALUES per l\'indicatore ' . $nomeIndicatore );
				continue;
			}

			$translate = FARCHIONI_INDICATORI_TRANSLATIONS_VALUES[ $nomeIndicatore ];
			$status = $product->add_meta_data( $metaToTranslate, $translate, true );

			// error_log('add_meta_data( '.$metaToTranslate.', '. $translate . ', true)' . ' Status: ' . var_export($status, true));
		}
	}
	// save the updates
	$product->save();
}

/**
 *  Aggiorna slug prodotto uguale a quella attualmente live
 * @param WC_Product $product
 * @param array      $data Raw CSV data.
 */
function farchioni_update_product_slug( WC_Product $product, $data ) {
	// get the product slug
	$oldSlug = $product->get_meta( 'old_product_slug', true );

	if ( $oldSlug === '' ) {
		error_log( 'Old slug is empty!' );
		return $product;
	}

	$product->set_slug( $oldSlug );
	// $product->delete_meta_data($meta_name);

	// $translatedProductId = apply_filters( 'wpml_object_id', $product->getID(), 'post', false, 'en' );

	return $product;
}

/**
 * Importa slug prodotto uguale a quella attualmente live sulla lingua inglese
 */
function farchioni_translate_product_slug( $translatedProduct ) {
	// Get the product
	$product = wc_get_product( $translatedProduct->get_id() );

	if ( ! $product instanceof WC_Product ) {
		// error_log('farchioni_translate_product_slug: product not WC_Product' . $translatedProduct->get_id());
		return;
	}

	// error_log('farchioni_translate_product_slug: ' . $translatedProduct->get_id());

	$originalProductId = apply_filters( 'wpml_object_id', $product->get_id(), 'product', false, 'it' );
	$originalProduct = wc_get_product( $originalProductId );

	if ( ! $originalProduct instanceof WC_Product ) {
		// error_log('farchioni_translate_product_slug: not WC_Product' . $originalProductId);
		return;
	}

	$oldSlug = $originalProduct->get_meta( 'old_product_slug_en', true );

	if ( $oldSlug === '' ) {
		// error_log('farchioni_translate_product_slug: slug empty' . $originalProductId);
		return;
	}

	// error_log('farchioni_translate_product_slug: ' . $oldSlug);

	$product->set_slug( $oldSlug );
	$product->save();
}
