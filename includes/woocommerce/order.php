<?php
/**
 * HOOKS
 */
add_action( 'woocommerce_thankyou', 'farchioni_update_order_status' );

/**
 * FUNCTIONS
 */

 /**
  * HACK: Re-imposta lo stato 'processing' degli ordini appena arrivati
  * così da assicurarsi che Shippy Pro si prenda i nuovi ordini
  */
function farchioni_update_order_status( $order_id )
{
    try {
        if ( ! $order_id ) {
            return;
        }

        $order = wc_get_order( $order_id );

        if ( ! $order ) {
            return;
        }

        $status = $order->get_status();

        error_log( "THANK YOU PAGE: Ordine " . $order->get_id() . ": stato attuale = '$status'" );

        if ( $status === 'pending' || $status === 'processing' ) {
            error_log( "THANK YOU PAGE: Refresh dello stato dell'ordine in 'processing'" );
            $order->update_status( 'processing' );
        }
    } catch (\Exception $e) {
        error_log( "Problema riscontrato nell'esecuzione di farchioni_update_order_status" );
    }
}
