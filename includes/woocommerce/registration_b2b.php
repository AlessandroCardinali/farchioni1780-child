<?php
/**
 * Filtri e funzonni per la registrazione di clienti b2b, con Gravity forms
 *
 */


// ID della form di registrazione b2b
$b2b_form_id = '6';

// prepopulate b2b form
add_filter( 'gform_pre_render_'. $b2b_form_id,               'farchioni_prepopulate_b2b_form' );
add_filter( 'gform_pre_validation_'. $b2b_form_id,           'farchioni_prepopulate_b2b_form' );
add_filter( 'gform_pre_submission_filter_'. $b2b_form_id,    'farchioni_prepopulate_b2b_form' );
add_filter( 'gform_admin_pre_render_'. $b2b_form_id,         'farchioni_prepopulate_b2b_form' );
// custom validation b2b form
add_filter( 'gform_validation_'. $b2b_form_id, 'farchioni_validate_b2b_form' );
// set wcb2b_status == -1 after activation
add_action( 'gform_activate_user', 'farchioni_set_wcb2b_status_after_activation', 10, 3 );
add_action('wp', 'custom_maybe_activate_user', 9);

/**
* Gravity Forms Custom Activation Template
* http://gravitywiz.com/customizing-gravity-forms-user-registration-activation-page
*/
function custom_maybe_activate_user() {

	// 20211014 AGGIORANTO DA GRAVITY WIZ

	$template_path    = STYLESHEETPATH . '/gravity-forms/activate.php';
	$is_activate_page = isset( $_GET['page'] ) && $_GET['page'] === 'gf_activation';
	$is_activate_page = $is_activate_page || isset( $_GET['gfur_activation'] ); // WP 5.5 Compatibility

	if ( ! file_exists( $template_path ) || ! $is_activate_page ) {
		return;
	}

    require_once( $template_path );

    exit();
}

/**
 * Prepopulate b2b form
 * - populate countries drop down (field must have CUSTOMER_PAESE class)
 * - populate partita iva conditional rules (field must have CUSTOMER_PIVA class)
 */
function farchioni_prepopulate_b2b_form( $form ) {

    $field_country_id = '0';

    foreach ( $form['fields'] as $field ) {

        // prepopulate countries with WC Countries
        if ( $field->type === 'select' && strpos( $field->cssClass, 'CUSTOMER_PAESE' ) != false ) {

            $field_country_id = $field->id;

            global $woocommerce;
            $countries_obj   = new WC_Countries();
            $WC_countries   = $countries_obj->__get('countries');

            $choices = array();

            foreach ( $WC_countries as $key => $val ) {
                if ( $key === 'IT' ){
                    $choices[] = array( 'text' => $val, 'value' => $key, 'isSelected' => true );
                } else {
                    $choices[] = array( 'text' => $val, 'value' => $key );
                }

            }

            $field->placeholder = __( 'Select your Country', 'farchioni1780-child' );
            $field->choices = $choices;

        }

        // dynamic conditional logic on partita iva,
        // we show it only if country is in EU (from WC eu_countries array)
        // and if we have a country field id
        if ( $field->type === 'text' && strpos( $field->cssClass, 'CUSTOMER_PIVA' ) != false && $field_country_id != '0') {

            global $woocommerce;
            $eu_countries = $woocommerce->countries->get_european_union_countries();

            $rules = array();

            foreach( $eu_countries as $country_code ){
                $rules[] = array(
                            'fieldId' => $field_country_id,
                            'operator' => 'is',
                            'value' => $country_code
                );
            }

            $field->conditionalLogic = array(
                'actionType' => 'show',
                'logicType' => 'any',
                'rules' => $rules
            );

        }

    }

    return $form;
}


/**
 * Custom validation for form b2b
 */
function farchioni_validate_b2b_form( $validation_result ){

    $form = $validation_result['form'];

    // get id of fields we want to validate by on field classes
    foreach ( $form['fields'] as $field ) {

        if ( $field->type === 'select' && strpos( $field->cssClass, 'CUSTOMER_PAESE' ) != false ) {
            $field_country_id = $field->id;
        }
        if ( $field->type === 'text' && strpos( $field->cssClass, 'CUSTOMER_PIVA' ) != false ) {
            $field_partita_iva_id = $field->id;
        }

    }

    // get form country code and vat number
    $country_code   = rgpost( 'input_' . $field_country_id );
    $vat_number     = rgpost( 'input_' . $field_partita_iva_id );

    global $woocommerce;
    $eu_countries = $woocommerce->countries->get_european_union_countries();

    $is_UE = in_array( $country_code, $eu_countries, true );

    $vat_valid = true;

    // validation vat number only for EU Countries
    if ( $is_UE ) {

        // if vat is empty, not valid
        if ( $vat_number == '' ) {
            $vat_valid = false;
            $validation_result['is_valid'] = false;
            $validation_message = __( 'Please insert yout VAT Number', 'farchioni1780-child' );
        } else {
			// Se i primi due caratteri dell'IVA sono un country code, not valid
			if ( in_array( strtoupper( substr( $vat_number, 0, 2 ) ), $eu_countries, true ) ) {
                $vat_valid = false;
                $validation_result['is_valid'] = false;
				$validation_message = __( 'Invalid VAT Number: please do not include the country code in the VAT number', 'farchioni1780-child' );
			} else {
                // VIES check
                $status = array();
                $vies_timeout = 20;
                try {
                    $result = ( new Idearia\Vies() )->check_vies( $country_code, $vat_number, $status, $vies_timeout );
                } catch (\Throwable $e) {
                    error_log( __FILE__ . ':' . __LINE__ . ' Il controllo Vies al momento non risponde. Procediamo come se la risposta fosse stata positiva' );
                    $result = true;
                }
                // if VIES check not true, not valid
                if ( true != $result ) {
                    $vat_valid = false;
                    $validation_result['is_valid'] = false;
                    $validation_message = __( 'Invalid VAT Number', 'farchioni1780-child' );
                }
            }

        }

        // find var number field and marking it as failed validation
        foreach( $form['fields'] as $field ) {
            if ( $field->id == $field_partita_iva_id && $vat_valid == false ) {
               $field->failed_validation = true;
               $field->validation_message = $validation_message;
               break;
            }
       }
	}

    //Assign modified $form object back to the validation result
    $validation_result['form'] = $form;
    return $validation_result;

}


/**
 * set wcb2b_status == -1 after user activation
 */
function farchioni_set_wcb2b_status_after_activation( $user_id, $user_data, $signup_meta ){

    update_user_meta( $user_id, 'wcb2b_status', intval( '-1' ) );

}
