<?php

/**
 * La tab di netreviews è chiamata due volte
 *  1. Dal filtro add_filter('woocommerce_product_tabs', 'ntav_add_our_review_tab', 98);
 *  2. Dentro il file woocommerce/single-product/tabs/tabs.php:ntav_netreviews_tab()
 * Questa doppia chiamata genera due volte il json-ld del aggregateRating e quindi Google si lamenta
 */

remove_filter('woocommerce_product_tabs', 'ntav_add_our_review_tab', 98);