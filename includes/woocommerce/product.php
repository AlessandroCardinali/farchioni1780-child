<?php

/**
 * HOOKS
 */
add_action( 'woocommerce_before_shop_loop_item_title', 'farchioni_add_variation_box' ); // Add variation select in product loop / archive page
add_action( 'woocommerce_before_shop_loop_item_title', 'farchioni_print_product_brand' ); // Add brand name in product loop / archive page
add_filter( 'woocommerce_cart_item_name', 'farchioni_add_product_brand_to_cart_item', 10, 3 ); // Add brand name in cart loop
add_action( 'woocommerce_single_product_summary', 'farchioni_print_product_brand', 0 ); // Add brand name in product page
add_filter( 'woocommerce_sale_flash', 'farchioni_custom_sale', 10, 3 ); // Add sale custom text
add_action( 'woocommerce_single_product_summary', 'farchioni_print_product_availability', 1 ); // Print product availability
// add_filter( 'woocommerce_available_variation', 'farchioni_cost_per_liter', 15, 3 ); // Print prezzo al litro
add_action( 'woocommerce_single_product_summary', 'farchioni_print_product_payoff_line', 35 ); // Add brand name in product page
//add_action( 'init', 'farchioni_remove_parent_theme_strange_wrap' ); // Template Bugfix
add_shortcode( 'product_min_price', 'farchioni_product_min_price_shortcode' ); // Shortcode for beste selers
add_filter( 'woocommerce_get_price_suffix', 'farchioni_price_suffix_for_professionals_users', 99, 4 ); // Change price suffix if product is vat free

// Move woocommerce_show_product_sale_flash from woocommerce_single_product_summary to woocommerce_before_single_product_summary
add_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 20 );

if ( function_exists( 'ntav_netreviews_parse_request' ) ) {
	add_action( 'init', 'move_netreviews_after_product_title'); // move netreview after product title
}


// set_theme_mod('zoo_enable_product_sale', false); // usato pir disabilitare il sale

/**
 * FUNCTIONS
 */

/**
 * Netreviews (Recensioni Verificate) puts reviews link at the end of the summary in single product page
 * We move it right after the product title
 */

function move_netreviews_after_product_title() {
	remove_action( 'woocommerce_single_product_summary', 'ntav_netreviews_product_rating', 31 );
	add_action('woocommerce_single_product_summary', 'ntav_netreviews_product_rating', 10);
}

/**
 * Print product brand to the single item inside loop
 */
function farchioni_print_product_brand() {
	global $product;
	echo farchioni_product_brand( $product->get_id() ); //phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
}

/**
 * Print product brand inside the cart
 */
function farchioni_add_product_brand_to_cart_item( $productName, $cartItem, $cartItemKey ) {
	return farchioni_product_brand( $cartItem['product_id'] ) . ' ' . $productName;
}

/**
 * Get product brand from database
 */
function farchioni_product_brand( $productId ) {
	// $brand = get_post_meta($productId, '_brand', true);
	$brand = get_field( 'brand_produttore', $productId );

	// No brand found, let's assume is Farchioni
	if ( $brand === '' ) {
		return '';
	}

	return '<div class="farchioni-brand">' . $brand . '</div>';
}

/**
 * Add box with link to variations for each single item inside loop
 */
function farchioni_add_variation_box() {
	global $product;
	$html = []; //phpcs:ignore Generic.Arrays.DisallowShortArraySyntax.Found
	$variation = $product->get_children();
	$html[] = '<div class="farchioni-variation-cta">';

	foreach ( $variation as $variationId ) {
		$productVariation = wc_get_product( $variationId );

		if ( ! $productVariation->variation_is_visible() ) {
			continue;
		}

		$formato = $productVariation->get_attribute( 'pa_formato' );
		$url = get_permalink( $variationId );

		$html[] = '<span class="variation"><a href="' . $url . '">' . $formato . '</a></span>';
	}

	$html[] = '</div>';

	/*
	 * Count if we have more than one variation,
	 * the numer is 3 because the first and last elements of the array are the div
	 */
	if ( count( $html ) > 3 ) {
		echo implode( $html ); //phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	}

}

/**
 * Replace the default sale label
 */
function farchioni_custom_sale( $html, $post, $product ) {

	$showLabel = farchioni_product_is_on_sale( $product, FARCHIONI_SOGLIA_SCONTO_PER_OFFERTE );

	if ( $showLabel === false ) {
		return '';
	}

	$percentage = farchioni_percentuale_sconto_etichetta( $product );

	$labelText = FARCHIONI_LABEL_OFFERTE;

	if ( is_numeric( $percentage ) ) {
		$labelText .= " -" . $percentage . "%";
	}

	return str_replace( __( 'Sale!', 'woocommerce' ), $labelText, $html );
}

/**
 * Print the pay-off line inside each product
 * The pay-off line is the text line below the add to cart button
 */
function farchioni_print_product_payoff_line() {
	global $product;
	$payOff = get_field( 'sottotitolo', $product->get_id() );
	echo '<div class="product-pay-off">' . $payOff . '</div>'; //phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
}

/**
 * Print a label if the product is available or not
 */
function farchioni_print_product_availability() {
	global $product;

	$status = __( 'Non disponibile', 'farchioni-child' );
	$class = 'product-availability-status';

	if ( $product->get_stock_status() === 'instock' ) {
		$status = __( 'Disponibile', 'farchioni-child' );
		$class .= ' available';
	}

	echo '<span class="' . $class . '">' . $status . '</span>'; //phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
}

/**
 * Get the meta `prezzo_al_litro` and add it to the array of variation data
 * To make it work you have to add (inside: woocommerce/single-product/add-to-cart/variation.php ):
 * <div class="farchioni-custom-variation-cost_per_liter">{{{ data.variation.cost_per_liter }}}</div>
 */
function farchioni_cost_per_liter( $array, $obj, $variation ) {
	$costPerLiter = get_post_meta( $variation->get_id(), 'prezzo_al_litro' );
	$array['cost_per_liter'] = $costPerLiter;
	return $array;
}

/**
 * This wrap cause some errors in display new thing after the add to cart button
 * I think the problem is generated because the `zoo_close_wrap_qty_template` is called 2 times,
 * and this cause the summary div to close to fast
 */
function farchioni_remove_parent_theme_strange_wrap() {
	remove_action( 'woocommerce_before_add_to_cart_quantity', 'zoo_open_wrap_qty_template', 0 );
	remove_action( 'woocommerce_after_add_to_cart_button', 'zoo_close_wrap_qty_template', 5 );
}


/**
 * Get the product minimim price to print inside the best sellers blocks
 */
function farchioni_product_min_price_shortcode( $atts ) {
	$params = shortcode_atts( [ 'product_id' => null ], $atts ); //phpcs:ignore Generic.Arrays.DisallowShortArraySyntax.Found
	$product = wc_get_product( $params['product_id'] );

	if ( ! $product ) {
		return null;
	}

	$price = wc_price( $product->get_price() );
	echo $price; //phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
}


/**
 * Remove price suffix when product is vat free
 */
function farchioni_price_suffix_for_professionals_users( $html, $product, $price, $qty ) {

	// if user is not logged in, use the default value
	if ( ! is_user_logged_in() ) {
		return $html;
	}

	$userGroupId = get_the_author_meta( 'wcb2b_group', get_current_user_id() );

	// user does not have a gropu, ex: administrator
	if ( ! is_numeric( $userGroupId ) ) {
		return $html;
	}

	$groupTaxExemption = get_post_meta( $userGroupId, 'wcb2b_group_tax_exemption', true );

	if ( $groupTaxExemption === '1' ) {
		return '<small class="woocommerce-price-suffix">' . __( 'Taxes excluded', 'farchioni1780-child' ) . '</small>';
	}

	return $html;
}

/**
 * Ritorna la percentuale di sconto del prodotto come un
 * numero intero da 0 a 100.
 *
 * Per i prodotti variabili, ritorna la percentuale di sconto
 * più altra tra le variazioni.
 */
function farchioni_percentuale_sconto_etichetta( $product ) {
	if ( $product->is_type( 'variable' ) ) {
		$percentages = array();
		$prices = $product->get_variation_prices();
		foreach( $prices['price'] as $key => $price ) {
			// Only on sale variations
			if( $prices[ 'regular_price' ][ $key ] !== $price ) {
				// Calculate and set in the array the percentage for each variation on sale
				$percentages[] = round( 100 - ( $prices[ 'sale_price' ][ $key ] / $prices[ 'regular_price' ][ $key ] * 100 ) );
			}
		}
		$percentage = max( $percentages );
	} else {
		$regular_price = (float) $product->get_regular_price();
		$sale_price    = (float) $product->get_sale_price();
		$percentage    = round( 100 - ($sale_price / $regular_price * 100 ) );
	}
	if ( ! is_numeric( $percentage ) ) {
		return '';
	}
	return $percentage;
}


// NUOVE FUNZIONI 2022
// RICHIESTA> in pagina listing mostrare i prezzi diversi per B2C e B2B, con la sola indicazione del prezzo minimo.
// -- rimuovo prezzo di default, controllo lo status utente, quindi mostro il prezzo appropriato.
// remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
// add_action( 'woocommerce_after_shop_loop_item_title', 'farchioni_template_loop_price', 9 );
function farchioni_template_loop_price(){

	if (is_b2b_user()){
		echo  'ciao B2B';
	} else {
		echo 'ciao B2C';
	}
}



// mostra solo il prezzo più basso in B2C e in B2B
// derivato da: https://wpglorify.com/show-lowest-price-woocommerce-variable-products/
add_filter( 'woocommerce_variable_sale_price_html', 'farchioni_variation_price_format', 100, 2 );
add_filter( 'woocommerce_variable_price_html', 'farchioni_variation_price_format', 100, 2 );


function farchioni_variation_price_format( $price, $product ) {

	$price = $price_pre = $price_after ='';
	$prezzi = array();

	$variations = $product->get_available_variations();

	foreach($variations as $prod){
		$prezzi[] = [ 'id'=>$prod['variation_id'],'price'=> $prod['display_price']];
	}

	// global $post;
	// if ($post->ID == 120664){
	// 	send_to_console($prezzi);
	// }

	usort($prezzi, function ($a, $b){
	  return $a["price"] <=> $b["price"]; // almeno php 7
	  //return strcmp($a["price"], $b["price"]);
	});

//
// 	if ($post->ID == 120664){
// 		send_to_console($prezzi);
// 		$reg_prices = $product->get_variation_prices( false ); // array multidimensionale con chiavi "price", "regular_price", "sale_price"
// 		send_to_console($reg_prices);
// 	}



	if (! is_b2b_user() ){

		if ( count( $product->get_available_variations() )>= 2 ) {
			$price_pre =  '<small>'.__( 'from', 'farchioni1780-child' ).'</small>';
			$variation = wc_get_product($prezzi[0]['id']);
			//$attributes = $variation->get_variation_attributes();
			//$cosa = str_replace('-', ' ',$attributes['attribute_pa_formato']);

			// Alessandro 9.11.2022
			// Formattazione variazione con maiuscole minuscole
			$attributes = $variation->get_attribute( 'pa_formato' );
			$cosa = $attributes;

			if ( ICL_LANGUAGE_CODE == 'en' ) {
				if ( substr($cosa, -3) == 'eng' || substr($cosa, -2) == 'en' ){
					$cosa = substr($cosa, 0, -3);
				}
			}

			$price_after = '<small>'.$cosa.'</small>';
			// $price_after = '<small>'.str_replace('-', ' ',$attributes['attribute_pa_formato']).'</small>';
		}

		$price = $price_pre. wc_price( $prezzi[0]['price']). $product->get_price_suffix(). $price_after;

	} else {

		$product_group_tier_prices = get_post_meta( $prezzi[0]['id'], 'wcb2b_product_group_tier_prices', true );

		if (is_array($product_group_tier_prices) && count($product_group_tier_prices) >=1 ){
			$customer_group_id = get_option( 'wcb2b_guest_group' );
			if ( is_user_logged_in() ) {
				$customer_group_id = get_the_author_meta( 'wcb2b_group', get_current_user_id() );
			}
			$tier_prices = $product_group_tier_prices[ $customer_group_id ];
			// se tier prices non  è array riprendiamo il prezzo singolo
			if (is_array($tier_prices)){
				$b2b_unit_qty = get_post_meta($prezzi[0]['id'], 'b2b_unit_qty', true ) ?: 1;
				$price = '<small>'.__( 'from', 'farchioni1780-child' ).'</small>'. wcb2b_display_tier_price( end($tier_prices) / $b2b_unit_qty) .'<small>'. __('each piece/bottle', 'farchioni1780-child').'</small>';
			} else {
				$price =  '<small>'.__( 'from', 'farchioni1780-child' ).'</small>'. wc_price( $prezzi[0]['price'] ) . $product->get_price_suffix();
			}

		} else {
			if ( count( $variations )>= 2 ) {
				$price =  '<small>'.__( 'from', 'farchioni1780-child' ).'</small>';
			}
			$price .= wc_price( $prezzi[0]['price'] ) . $product->get_price_suffix();
		}

	}

	return $price;
}


/*
*  DISATTIVATA e ottimizzata dalla precedente
*/
// function farchioni_variation_price_format_old( $price, $product ) {
// 	if (! is_b2b_user() ){
// 		// prendo il più basso tra il minimo da listino e il minimo in offerta
// 		$price = $price_pre = $price_after ='';
// 		$prezzi = array();
//
// 		// PROBLEMA: vanno esclusi gli id delle variazioni B2B: $product->get_variation_prices( false ) non funziona
// 		$reg_prices = $product->get_variation_prices( false ); // array multidimensionale con chiavi "price", "regular_price", "sale_price"
// 		$prezzi = [
// 			[
// 				'id'    => key( $reg_prices['regular_price'] ),
// 				'price' => current( $reg_prices['regular_price'] ),
// 		    ],
// 			[
// 				'id'    => key( $reg_prices['sale_price'] ),
// 				'price' => current( $reg_prices['sale_price'] ),
// 			],
// 		];
//
// 		$variations = $product->get_available_variations();
// 		$prezzi = array();
//
// 		foreach($variations as $prod){
// 			$prezzi[] = [ 'id'=>$prod['variation_id'],'price'=> $prod['display_regular_price']];
// 		}
//
// 		usort($prezzi, function ($a, $b){
// 		  return strcmp($a["price"], $b["price"]);
// 		});
//
// 		if ( count( $product->get_available_variations() )>= 2 ) {
// 			$price_pre =  '<small>'.__( 'from', 'farchioni1780-child' ).'</small>';
// 			$variation = wc_get_product($prezzi[0]['id']);
// 			$attributes = $variation->get_variation_attributes();
// 			$price_after = '<small>'.str_replace('-', ' ',$attributes['attribute_pa_formato']).'</small>';
// 		}
// 		$price = $price_pre. wc_price( $prezzi[0]['price']). $product->get_price_suffix(). $price_after;
//
//
//
//
//
//
//
// 		// $minimo_listino = $product->get_variation_regular_price( 'min', true );
// 		// $minimo_vendita = $product->get_variation_price( 'min', true );
// 		// $prezzi_minimi = array( $product->get_variation_regular_price( 'min', true ), $product->get_variation_price( 'min', true ));
// 		// sort( $prezzi_minimi );
// 		// if ( count( $product->get_available_variations() )>= 2 ) {
// 		// 	$price =  '<small>'.__( 'from', 'farchioni1780-child' ).'</small>';
// 		// }
// 		// $price .= wc_price( $prezzi_minimi[0]). $product->get_price_suffix();
//
// 	} else {
// 		// nel b2b probabilmente non vengono filtrati in modo corretto i risultati dei metodi get_variation_regular_price e compare, quindi si fa a mano.
//
// 		$price = '';
//
// 		$variations = $product->get_available_variations();
// 		$prezzi = array();
//
// 		foreach($variations as $prod){
// 			$prezzi[] = [ 'id'=>$prod['variation_id'],'price'=> $prod['display_regular_price']];
// 		}
//
// 	   // send_to_console($prezzi);
// 		usort($prezzi, function ($a, $b){
// 		  return strcmp($a["price"], $b["price"]);
// 		});
// 		// send_to_console($prezzi);
//
// 		$product_group_tier_prices = get_post_meta( $prezzi[0]['id'], 'wcb2b_product_group_tier_prices', true );
//
// 		if (is_array($product_group_tier_prices) && count($product_group_tier_prices) >=1 ){
// 			$customer_group_id = get_option( 'wcb2b_guest_group' );
// 			if ( is_user_logged_in() ) {
// 				$customer_group_id = get_the_author_meta( 'wcb2b_group', get_current_user_id() );
// 			}
// 			$tier_prices = $product_group_tier_prices[ $customer_group_id ];
// 			// se tier prices non  è array riprendiamo il prezzo singolo
// 			if (is_array($tier_prices)){
// 				$b2b_unit_qty = get_post_meta($prezzi[0]['id'], 'b2b_unit_qty', true ) ?: 1;
// 				$price = '<small>'.__( 'from', 'farchioni1780-child' ).'</small>'. wcb2b_display_tier_price( end($tier_prices) / $b2b_unit_qty) .'<small>'. __('each piece/bottle', 'farchioni1780-child').'</small>';
// 			} else {
// 				$price =  '<small>'.__( 'from', 'farchioni1780-child' ).'</small>'. wc_price( $prezzi[0]['price'] ) . $product->get_price_suffix();
// 			}
//
// 		} else {
// 			if ( count( $variations )>= 2 ) {
// 				$price =  '<small>'.__( 'from', 'farchioni1780-child' ).'</small>';
// 			}
// 			$price .= wc_price( $prezzi[0]['price'] ) . $product->get_price_suffix();
// 		}
//
// 	}
//
// 	return $price;
// }


