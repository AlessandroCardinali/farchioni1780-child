<?php

/**
 *  note da idearia a fosforica
 *
 *	myfield13_field CF
 *	myfield8_field IVA
 *	myfield15_field Pec o SDI
 *
 *
 */

// PER GLI UTENTI B2B RENDO READONLY SOCIETA, IVA, RICHIESTA DI FATTURA, NAZIONE, Telefono, nome e cognome
add_action('woocommerce_checkout_fields','fosforica_b2b_fields',99999,1);
function fosforica_b2b_fields($checkout_fields){
	if ( is_b2b_user() ){
		foreach ( $checkout_fields['billing'] as $key => $field ){
			if ( in_array($key, ['billing_company', 'billing_myfield8', 'billing_myfield12', 'billing_country','billing_phone','billing_first_name','billing_last_name','billing_email']) ){
				$checkout_fields['billing'][$key]['custom_attributes'] = array('readonly'=>'readonly');
			}
		}
		$country = get_user_meta( get_current_user_id(), 'billing_country', true );
		// https://stackoverflow.com/questions/52279966/make-checkout-country-dropdown-readonly-in-woocommerce
		WC()->customer->set_billing_country($country);
		WC()->customer->set_shipping_country($country);

		$checkout_fields['billing']['billing_country']['custom_attributes'] = array('disabled'=>'disabled');
		$checkout_fields['shipping']['shipping_country']['custom_attributes'] = array('disabled'=>'disabled');
	}
	return $checkout_fields;
}


add_action('woocommerce_after_order_notes', 'fosforica_b2b_country_hidden_field');
function fosforica_b2b_country_hidden_field($checkout){
	if ( is_b2b_user() ){
		$country = get_user_meta( get_current_user_id(), 'billing_country', true );
		echo '<input type="hidden" class="input-hidden" name="billing_country"  value="'.$country.'">';
		echo '<input type="hidden" class="input-hidden" name="shipping_country"  value="'.$country.'">';
	}
}


add_action( 'login_form_bottom', 'add_lost_password_link' );
function add_lost_password_link() {
	// $redirect
	return '<a id="password_recover" href="'.wp_lostpassword_url().'">'. __( 'Lost your password?', 'woocommerce' ).'</a>';
	//return '<a href="/wp-login.php?action=lostpassword">Lost Password?</a>';
}

// https://stackoverflow.com/questions/45200360/reorder-woocommerce-checkout-fields
// previeni riordinamento
// add_filter( 'woocommerce_default_address_fields', 'custom_override_default_locale_fields' );
// function custom_override_default_locale_fields( $fields ) {
// 	$fields['state']['priority'] = 5;
// 	$fields['address_1']['priority'] = 6;
// 	$fields['address_2']['priority'] = 7;
// 	$fields['postcode']['priority'] = 8;
// 	return $fields;
// }

add_filter( 'woocommerce_get_country_locale', function( $locale ) {
	foreach ( $locale as $country_code => $locale_fields ) {
		foreach ( $locale_fields as $field_key => $field_options ) {
			if ( isset( $field_options['priority'] ) ) {
				unset( $field_options['priority'] );
			}

			$locale[ $country_code ][ $field_key ] = $field_options;
		}
	}

	return $locale;
} );



/**
 * PRE FOSFORICA:
 *
 *
 * HOOKS
 */
add_filter( 'woocommerce_billing_fields', 'farchioni_billing_details_obbligatori', 99999 );
add_action( 'woocommerce_checkout_update_order_review', 'bbloomer_taxexempt_checkout_based_on_zip' );
add_action( 'woocommerce_checkout_process', 'my_custom_checkout_field_process' );
//add_filter( 'woocommerce_cart_shipping_method_full_label', 'bbloomer_remove_shipping_label', 10, 2 );
add_filter( 'gettext', 'ld_custom_paypal_button_text', 20, 3 );
add_action( 'wc_shipment_tracking_get_providers', 'wc_shipment_tracking_add_custom_provider' );
add_filter( 'wc_shipment_tracking_get_providers', 'custom_shipment_tracking' );
add_filter( 'woocommerce_package_rates', 'my_hide_shipping_when_free_is_available', 100 );
add_filter( 'woocommerce_email_headers', 'farchioni_admin_emails_header', 10, 3 ); // Add reply-to & BCC fields to WooCommerce emails
add_action('woocommerce_checkout_process', 'farchioni_force_shipping_country');
add_filter( 'default_checkout_billing_state', '_return_null' ); // Imposta a null la provincia di default

/**
 * FUNCTIONS
 */

/**
 * Rendi obbligatori i campi di billing.
 *
 * Impostiamo la priorità del filtro come altissima
 * per assicurarci di essere gli ultimi a modificare
 * i campi.
 *
 * Nota bene 1: Nel tema child originariamente era impostato
 * uno stesso filtro con priorità 1000 che rendeva non-required
 * il campo billing_address_1, perché?
 *
 * Nota bene 2: La versione 4.2.5 del plugin WooCommerce
 * di Checkout Manager usa priorità 10, nelle versioni
 * successive viene aumentata.
 *
 * Nota bene 3: Questo sarebbe il posto dove levare la flag
 * "required" ai campi di fatturazione per le aziende (iva, sdi,
 * cf, company) nell'eventualità in cui il cliente non richiede
 * fattura. Al momento questo aspetto è gestito in maniera oscura
 * nel jQuery in form-checkout.php inizializzando con dei trattini
 * (dash -) evitando quindi il check required a backend.
 */

function farchioni_billing_details_obbligatori( $address_fields ) {

	$address_fields['billing_address_1']['required'] = true;
	$address_fields['billing_postcode']['required'] = true;
	$address_fields['billing_city']['required'] = true;
	// $address_fields['billing_state']['required'] = true;

	return $address_fields;
}




/**
 * Filtro sparato dopo che l'utente ha fatto il
 * submit del modulo di checkout
 *
 * https://docs.woocommerce.com/document/tutorial-customising-checkout-fields-using-actions-and-filters/
 */
function my_custom_checkout_field_process() {
	$post_data_string = file_get_contents( 'php://input' );
	imposta_imposte( $post_data_string );
}

/**
 * Filtro sparato dopo il change di un qualsiasi campo
 * del checkout, solitamente usato per la validazione.
 *
 * https://docs.woocommerce.com/wc-apidocs/hook-docs.html
 */
function bbloomer_taxexempt_checkout_based_on_zip( $post_data_string ) {
	imposta_imposte( $post_data_string );
}

/**
 * Determina che imposta deve pagare l'acquirente sulla
 * base dei sui dati di fatturazione
 *
 * @param [string] $post_data_string
 * @return void
 */
function imposta_imposte( $post_data_string ) {

	global $woocommerce;

	// Debug
	// error_log( 'POST DATA = ' . print_r( $post_data_string, true ) );

	// Inizializza i valori che potrebbero essere cambiati
	$_SESSION['ship_exempt'] = false;
	$woocommerce->customer->set_is_vat_exempt( false );

	// Salva in un array i dati inseriti nel modulo
	$post_data = array();
	parse_str( $post_data_string, $post_data );

	// Estra i dati di fatturazione
	$campo_partita_iva = 'billing_myfield8';
	$vat_number = $post_data[ $campo_partita_iva ] ?? '';
	$country_code = $woocommerce->customer->get_shipping_country();

	write_log($country_code);

	// Il tema in form-checkout.php inizializza il campo partita IVA con un
	// trattino, quindi dobbiamo considerare il trattino come se fosse una
	// partita IVA vuota
	if ( $vat_number === '-' ) {
		$vat_number = '';
	}

	// Se la spedizione è nella UE...
	$eu_countries = farchioni_get_european_union_countries();
	$is_UE = in_array( $country_code, $eu_countries, true );

	if ( $is_UE ) {
		error_log( "$country_code È NELLA UE" );
		if ( $vat_number ) {
			// Se i primi due caratteri dell'IVA sono un country code,
			// ritorna errore
			if ( in_array( strtoupper( substr( $vat_number, 0, 2 ) ), $eu_countries, true ) ) {
				$errorMsg = 'Invalid VAT Number: please do not include the country code in the VAT number';
				if ( ICL_LANGUAGE_CODE === 'it' ) {
					$errorMsg = 'Numero di P.Iva errato: per cortesia non inserire il prefisso della nazione';
				}
				wc_add_notice( $errorMsg, 'error' );
				return;
			}
			// Controllo VIES
			$status = array();
			$vies_timeout = 20;
			try {
				$result = ( new Idearia\Vies() )->check_vies( $country_code, $vat_number, $status, $vies_timeout );
			} catch (\Throwable $e) {
				error_log( __FILE__ . ':' . __LINE__ . ' Il controllo Vies al momento non risponde. Procediamo come se la risposta fosse stata positiva' );
				$result = true;
			}
			// Se la partita IVA inserita è valida...
			if ( true === $result ) {
				error_log( "P.IVA $vat_number è valido" );
				// Se non spedisci in Italia...
				if ( $woocommerce->customer->get_shipping_country() !== 'IT' ) {
					// ... non paghi l'IVA
					$woocommerce->customer->set_is_vat_exempt( true );
					$_SESSION['ship_exempt'] = true;
					error_log( "P.IVA $vat_number ESENTE perché non italiano" );
				}
			} else { // Se la partita IVA inserita NON è valida...
				// ... mostra un messaggio di errore
				$errorMsg = 'Invalid VAT Number';
				if ( ICL_LANGUAGE_CODE === 'it' ) {
					$errorMsg = 'Numero di P.Iva non verificato';
				}
				wc_add_notice( $errorMsg, 'error' ); // valuta di usare $woocommerce->add_error
				error_log( "P.IVA $vat_number INVALIDA" );
			}
		}
	} else { // Se l'acquirente non spedisce nella UE...
		// ... non fargli pagare l'IVA
		$woocommerce->customer->set_is_vat_exempt( true );
		error_log( "$country_code è EXTRA UE => ESENTE" );
	}
}

/**
 * Nazioni della comunità europea ai fini IVA
 */
function farchioni_get_european_union_countries() {
	global $woocommerce;
	$eu_countries = $woocommerce->countries->get_european_union_countries();
	// Il Regno Unito è nella UE solo fino al 31/12/2020
	$last_second_of_2020 = 1609455540;
	if ( time() <= $last_second_of_2020 ) {
		$eu_countries[] = 'GB';
	}
	return $eu_countries;
}

/**
 * @snippet       Change shipping method labels @ WooCommerce Cart
 * @how-to        Watch tutorial @ https://businessbloomer.com/?p=19055
 * @sourcecode    https://businessbloomer.com/?p=484
 * @author        Rodolfo Melogli
 * @testedwith    WooCommerce 2.6.2
 */
function bbloomer_remove_shipping_label( $label, $method ) {

	$placeholder = 'Express courier: ';

	if ( ICL_LANGUAGE_CODE === 'it' ) {
		$placeholder = 'Corriere espresso: ';
	}

	$new_label = preg_replace( '/^.+:/', $placeholder, $label );

	return $new_label;
}

function ld_custom_paypal_button_text( $translated_text, $text, $domain ) {
	switch ( $translated_text ) {
		case 'Proceed to PayPal' :
		case 'Place order' :
		{
			$translated_text = __( 'Buy now', 'woocommerce' );
		}break;

		case 'Continua su PayPal' :
		case 'Effettua ordine' :
		{
			$translated_text = __( 'Acquista ora', 'woocommerce' );
		}break;

	}
	return $translated_text;
}




//Add Custom Providers Dynamically
/**
 * Adds custom provider to shipment tracking
 * Change the country name, the provider name, and the URL (it must include the %1$s)
 * Add one provider per line
 */
function wc_shipment_tracking_add_custom_provider( $providers ) {

	$providers['Italy']['SDA (let.vettura)'] = 'https://www.sda.it/wps/portal/Servizi_online/ricerca_spedizioni?locale=it&tracing.letteraVettura=%1$s';
	$providers['Italy']['MBE (num.sped)'] = 'https://www.mbe.it/it/tracking';
	//$providers['Italy']['BRT (rif.alfa)'] = 'https://vas.brt.it/vas/sped_ricDocMit_load.hsm?referer=sped_numspe_par.htm&KSU=0881497&DocMit=&rma=%1$s&RicercaDocMit=Ricerca';
	//$providers['Italy']['BRT (n.collo)'] = 'https://vas.brt.it/vas/sped_det_show.hsm?  referer=sped_numspe_par.htm&ChiSono=%1$s&ClienteMittente=&DataInizio=&DataFine=&RicercaChiSono=Ricerca';
	//$providers['Italy']['GLS ID Collo'] = 'https://www.gls-italy.com/?option=com_gls&view=track_e_trace&mode=search&numero_spedizione=%1$s&tipo_codice=id_collo';
	//$providers['Italy']['GLS Num. Spedizione'] = 'https://www.gls-italy.com/?option=com_gls&view=track_e_trace&mode=search&numero_spedizione=%1$s&tipo_codice=nazionale';
	//$providers['Italy']['BRT (n.sped)'] = 'https://vas.brt.it/vas/sped_det_show.hsm?referer=sped_numspe_par.htm&Nspediz=%1$s&RicercaNumeroSpedizione=Ricerca';

	// etc...

	return $providers;

}

//Hide Unused Shipping Providers
function custom_shipment_tracking( $providers ) {

	unset( $providers['Australia'] );
	unset( $providers['Austria'] );
	unset( $providers['Brazil'] );
	unset( $providers['Belgium'] );
	unset( $providers['Canada'] );
	unset( $providers['Czech Republic'] );
	unset( $providers['Finland'] );
	unset( $providers['France'] );
	unset( $providers['Germany'] );
	unset( $providers['Ireland'] );
	unset( $providers['India'] );
	unset( $providers['Netherlands'] );
	unset( $providers['Romania'] );
	unset( $providers['South African'] );
	unset( $providers['Sweden'] );
	unset( $providers['New Zealand'] );
	unset( $providers['United Kingdom'] );
	unset( $providers['United States']['FedEx'] );
	unset( $providers['United States']['FedEx Sameday'] );
	unset( $providers['United States']['UPS'] );
	unset( $providers['United States']['USPS'] );
	unset( $providers['United States']['OnTrac'] );
	unset( $providers['United States']['DHL US'] );
	unset( $providers['Italy']['BRT (Bartolini)'] );
	//unset($providers['Italy']['GLS ID Collo']);
	//unset($providers['Italy']['GLS Num. Spedizion']);
	unset( $providers['Italy']['DHL Express'] );

	return $providers;
}
/**
 * Hide shipping rates when free shipping is available.
 * Updated to support WooCommerce 2.6 Shipping Zones.
 *
 * @param array $rates Array of rates found for the package.
 * @return array
 */
function my_hide_shipping_when_free_is_available( $rates ) {
	$free = array();
	foreach ( $rates as $rate_id => $rate ) {
		if ( 'free_shipping' === $rate->method_id ) {
			$free[ $rate_id ] = $rate;
			break;
		}
	}
	return ! empty( $free ) ? $free : $rates;
}


// ===================================
// = WooCommerce Notification Emails =
// ===================================
/**
 * Add 'Reply-To' header to admin emails, so that the admin can reply
 * directly to the buyer; optionally add also BCC field.
 *
 * The $object argument can be any WooCommerce object (product, order,
 * user...) depending on the email ID.
 *
 * Possible email IDs can be extracted from filenames in the
 * woocommerce/includes/emails folder:
 *
 * - cancelled-order
 * - failed-order
 * - new-order
 * - customer-completed-order
 * - customer-invoice
 * - customer-new-account
 * - customer-note
 * - customer-on-hold-order
 * - customer-processing-order
 * - customer-refunded-order
 * - customer-reset-password
 *
 * Source: https://gist.github.com/coccoinomane/443d982ac5c3914b204506c717d27f6a
 */
function farchioni_admin_emails_header( $headers = '', $id = '', $object ) {

	// $bcc_email = 'notifiche@idearia.it';

	/* Add reply-to field to all emails sent to shop manager */
	if ( $id === 'new_order' || $id === 'failed_order' || $id === 'cancelled_order' ) {
		if ( method_exists( $object, 'get_billing_email' ) ) {
			$headers .= 'Reply-To: ' . $object->get_billing_email() . "\r\n";
		}
	}

	/* Add BCC address to all emails */
	if ( $bcc_email ) {
		$headers .= 'Bcc: ' . $bcc_email . "\r\n";
	}

	return $headers;
}

/**
 * Il paese di spedizione deve essere uguale al paese di fatturazione
 */
function farchioni_force_shipping_country() {

	// shipping address non abilitata
	if($_POST['ship_to_different_address'] !== '1') {
		return ;
	}

	// tutto ok, shipping e billing corrispondono
	if($_POST['billing_country'] === $_POST['shipping_country']) {
		return ;
	}

	// shipping e billing non corrispondono
    wc_add_notice( __( '<strong>Shipping Country</strong> must be the same as Billing Country', 'farchioni1780-child' ), 'error' );
}
