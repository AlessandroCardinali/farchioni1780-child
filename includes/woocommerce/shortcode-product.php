<?php
/**
 * HOOKS
 */

// Filter Offers query
add_filter( 'woocommerce_shortcode_products_query', 'farchioni_sale_shortcode_products' );

/**
 * FUNCTIONS
 */

/**
 * Filter the product query to show only the products that are on sale
 * The function will be called only if the page slug is offerte/offers
 * Check farchioni_product_is_on_sale() for more details
 */
function farchioni_sale_shortcode_products( $args ) {

	global $post;
	$page_slug = $post->post_name;

	if ( $page_slug !== 'offerte' && $page_slug !== 'offers' ) {
		return $args;
	}

	$args['posts_per_page'] = 50;

	$sale_products = wc_get_product_ids_on_sale();

	foreach ( $sale_products as $key => $productId ) {
		$product = wc_get_product( $productId );
		if ( farchioni_product_is_on_sale( $product, FARCHIONI_SOGLIA_SCONTO_PER_OFFERTE ) === false ) {
			unset( $sale_products[ $key ] );
			// echo 'Product was removed ' . $productId . '<br>';
		}
	}

	if ( array_key_exists('post__in', $args) && is_array( $args['post__in'] ) ) {
		$args['post__in'] = array_merge( $args['post__in'], $sale_products );
	} else {
		$args['post__in'] = $sale_products;
	}

	return $args;
}
