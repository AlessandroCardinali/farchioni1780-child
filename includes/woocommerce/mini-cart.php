<?php

/**
 * HOOKS
 */
add_action( 'init', 'remove_doma_free_shipping_notice'); // remove free shipping notice
if ( get_theme_mod( 'zoo_enable_free_shipping_notice', '1' ) == '1' ) { // e la metto prima del totale
    add_action( 'zoo_free_shipping_cart_notice', 'zoo_free_shipping_cart_notice', 5 );
}
add_filter( 'woocommerce_get_script_data', 'farchioni_woocommerce_get_script_data', 10, 2 );

/**
 * FUNCTIONS
 */

/**
 * Rimuovi la free shipping notice che sta prima dei bottoni
 */
function remove_doma_free_shipping_notice() {
    remove_action( 'woocommerce_widget_shopping_cart_before_buttons', 'zoo_free_shipping_cart_notice', 5 );
}

/**
 * Rimuovi il timeout della chiamata XHR get_cart_fragments
 */
function farchioni_woocommerce_get_script_data( $params, $handle ) {
    if ( $handle === 'wc-cart-fragments' ) {
        $params['request_timeout'] = 0;
    }
    return $params;
}
