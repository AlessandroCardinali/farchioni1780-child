<?php

/**
 * Aggiungi campi alla form di registrazione di Woocommerce
 * - nome
 * - cognome
 * - privacy
 * - newsletter
 * - brand preferito
 */

add_action( 'woocommerce_register_form_start', 'farchioni_add_name_woo_account_registration' ); // campi prima della mail+password
add_action( 'woocommerce_register_form', 'farchioni_add_acceptance_woo_account_registration' ); // campi dopo mail+password
add_filter( 'woocommerce_registration_errors', 'farchioni_validate_name_fields', 10, 3 ); // validazione campi
add_action( 'woocommerce_created_customer', 'farchioni_save_name_fields' ); // salvataggio campi

// aggiunge nome e cognome
function farchioni_add_name_woo_account_registration(){
	?>
	<p class="form-row form-row-first">
		<label for="billing_first_name"><?php _e( 'First name', 'woocommerce' ); ?> <span class="required">*</span></label>
		<input type="text" class="input-text" name="billing_first_name" id="reg_billing_first_name" value="<?php if ( ! empty( $_POST['billing_first_name'] ) ) esc_attr_e( $_POST['billing_first_name'] ); ?>" />
	</p>
	<p class="form-row form-row-last">
		<label for="billing_last_name"><?php _e( 'Last name', 'woocommerce' ); ?> <span class="required">*</span></label>
		<input  type="text" class="input-text" name="billing_last_name" id="reg_billing_last_name" value="<?php if ( ! empty( $_POST['billing_last_name'] ) ) esc_attr_e( $_POST['billing_last_name'] ); ?>" />
	</p>
	<div class="clear"></div>
	<?php
}

// aggiungi privacy, newsletter e brand preferito
function farchioni_add_acceptance_woo_account_registration() {

	$privacy_page_id = apply_filters( 'wpml_object_id', 5054, 'post' );
	$cookie_page_id = apply_filters( 'wpml_object_id', 5027, 'post' );

	$privacy_link = '<a href="'. get_permalink($privacy_page_id) .'" target="_blank" id="privacy_link_href">Privacy Policy</a>';
	$cookie_link = '<a href="'. get_permalink($cookie_page_id) .'" target="_blank" id="cookie_link_href">Cookies Policy</a>';
	?>

	<p class="form-row form-row-first checkbox">
		<label for="customer_accept_privacy">
			<input type="checkbox" name="customer_accept_privacy" id="customer_accept_privacy" />
			<span><?php printf( __( 'Ho letto ed accetto le condizioni di trattamento della %s & %s', 'farchioni1780-child' ), $privacy_link, $cookie_link ); ?> <span class="required">*</span></span>
		</label>
	</p>

	<p class="form-row form-row-last checkbox">
		<label for="customer_accept_newsletter">
			<input type="checkbox" name="customer_accept_newsletter" id="customer_accept_newsletter" />
			<span><?php printf( __( 'Do il mio consenso all’invio di comunicazioni commerciali secondo la %s', 'farchioni1780-child' ), $privacy_link ); ?></span>
		</label>
	</p>

	<div class="clear"></div>

	<p class="form-row form-row-wide">
		<label for="brand_preferito"><?php _e( 'Brand preferito', 'farchioni1780-child' ); ?><span class="required"></span></label>
		<select class="input-select" name="brand_preferito" id="brand_preferito">
			<option value="Farchioni">Farchioni</option>
			<option value="Terre de la Custodia">Terre de la Custodia</option>
			<option value="Mastri Birrai Umbri">Mastri Birrai Umbri</option>
		</select>
	</p>

	<?php $sendinblue_lang = ( ICL_LANGUAGE_CODE === 'it' )? 'ITA' : 'ENG';?>
	<input type="hidden" value="<?php echo $sendinblue_lang;?>" name="LANG"/>
	<input type="hidden" value="registration_form" name="FONTE_TXT"/>
	<input type="hidden" value="B2C" name="GRUPPO_TXT"/>

	<?php
}

// validazione campi
function farchioni_validate_name_fields( $errors, $username, $email ) {
	/**
	 * Controlla se stiamo nel checkout e salta la validazione.
	 *  In particolare questa validazione veniva chiamata se l'utente durante l'acquisto desiderava registrarsi ma la checkbox per `customer_accept_privacy` non esisteva.
	 */
	if ( is_checkout() ) {
		return $errors;
	}

	/**
	 * TODO: mostrare gli errori solo sulla pagina di registrazione dell'account `/my-account/`
	 * Un rapido test con questo non ha funzionato: https://wordpress.stackexchange.com/questions/288040/function-like-is-registration-page-to-check-if-current-page-is-registration-page
	 */

	if ( isset( $_POST['billing_first_name'] ) && empty( $_POST['billing_first_name'] ) ) {
		$errors->add( 'billing_first_name_error', __( '<strong>Error</strong>: First name is required!', 'farchioni1780-child' ) );
	}
	if ( isset( $_POST['billing_last_name'] ) && empty( $_POST['billing_last_name'] ) ) {
		$errors->add( 'billing_last_name_error', __( '<strong>Error</strong>: Last name is required!', 'farchioni1780-child' ) );
	}
	if ( ! isset( $_POST['customer_accept_privacy'] )  ) {
		$errors->add( 'billing_privacy_error', __( '<strong>Error</strong>: Privacy is required!', 'farchioni1780-child' ) );
	}

	return $errors;
}


// salva campi
function farchioni_save_name_fields( $customer_id ) {

	if ( isset( $_POST['billing_first_name'] ) ) {
		update_user_meta( $customer_id, 'billing_first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
		update_user_meta( $customer_id, 'first_name', sanitize_text_field($_POST['billing_first_name']) );
	}
	if ( isset( $_POST['billing_last_name'] ) ) {
		update_user_meta( $customer_id, 'billing_last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
		update_user_meta( $customer_id, 'last_name', sanitize_text_field($_POST['billing_last_name']) );
	}

	if ( isset( $_POST['brand_preferito'] ) ) {
		update_user_meta( $customer_id, 'customer_brand_preferito', sanitize_text_field( $_POST['brand_preferito'] ) );
	}

	if ( isset( $_POST['customer_accept_privacy'] ) ) {
		update_user_meta( $customer_id, 'customer_accept_privacy', sanitize_text_field( '1' ) );
	} else {
		update_user_meta( $customer_id, 'customer_accept_privacy', sanitize_text_field( '0' ) );
	}

	if ( isset( $_POST['customer_accept_newsletter'] ) ) {
		update_user_meta( $customer_id, 'customer_accept_newsletter', sanitize_text_field( '1' ) );
	} else {
		update_user_meta( $customer_id, 'customer_accept_newsletter', sanitize_text_field( '0' ) );
	}

}
