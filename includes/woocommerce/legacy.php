<?php


function get_all_variations_data( $product_id ) {

	$product = new WC_Product_Variable( $product_id );
	$variations = $product->get_available_variations();

	$retvalue = array();

	//Estraggo la variazione di default
	$default_value = $product->get_variation_default_attribute( 'pa_formato' );

	foreach ( $variations as $variation ) {

		$tmp = array();
		// aggiunto Fosforica B2B 2021
		// permette, nel quantity-input persnoalizzato, di avere un data-attribute con id della variazione
		$tmp['variation_id'] = $variation[ 'variation_id' ];
		$tmp['formato'] = $variation['attributes']['attribute_pa_formato'];
		$tmp['display_price'] = $variation['display_price'];
		$tmp['display_regular_price'] = $variation['display_regular_price'];

		$taxonomy = 'pa_formato';
		$taxonomy_label = get_taxonomy( $taxonomy )->labels->singular_name;
		$term_name = get_term_by( 'slug', $variation['attributes']['attribute_pa_formato'], $taxonomy )->name;

		$tmp['name'] = $term_name;

		$tmp['is_default'] = '0';

		// phpcs:ignore WordPress.PHP.StrictComparisons.LooseComparison
		if ( $default_value == $variation['attributes']['attribute_pa_formato'] ) {
			$tmp['is_default'] = '1';
		}

		$retvalue[] = $tmp;

	}

	return $retvalue;
}

//Product Detail Layout
if (!function_exists('zoo_woo_gallery_layout_single')) {
    function zoo_woo_gallery_layout_single($productId='')
    {
        if($productId!=''){
            $zoo_layout_single = get_post_meta($productId, 'zoo_single_gallery_layout', true);
        }else{
            $zoo_layout_single = get_post_meta(get_the_ID(), 'zoo_single_gallery_layout', true);
        }
        if ($zoo_layout_single == 'inherit' || $zoo_layout_single == '') {
            $zoo_layout_single = get_theme_mod('zoo_single_gallery_layout', 'vertical-gallery');
        }
        return $zoo_layout_single;
    }
}

//Single Product share
if (!function_exists('zoo_woo_enable_share')) {
    function zoo_woo_enable_share()
    {
        $zoo_status = false;
        if (get_theme_mod('zoo_single_share', '1') == 1) {
            $zoo_status = true;
        }
        return $zoo_status;
    }
}
