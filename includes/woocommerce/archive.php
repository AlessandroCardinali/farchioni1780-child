<?php

/**
 * HOOKS
 */
//add_filter( 'body_class', 'farchioni_add_archive_categories_to_body_class' );
//add_filter( 'body_class', 'farchioni_add_product_categories_to_body_class' );

// Sostituito nel css utilizzando le classi generate da wp "term-XXX"

/**
 * FUNCTIONS
 */

/**
 * Add categories ass body classes inside archive pages
 */
function farchioni_add_archive_categories_to_body_class( $classes ) {
	if ( ! is_archive( 'product_cat' ) ) {
		return $classes;
	}

	$category = get_queried_object();
	$prefix = 'farchioni-category-';

	// loop until we have only the parent category
	do {
		if (get_class( $category ) === 'WP_Term' && is_object($category) && $category->parent !== 0) {
			$category = $term = get_term_by( 'id', $category->parent, 'product_cat' );
			$classes[] = $prefix . $category->slug;
		}
	} while(get_class( $category ) === 'WP_Term' && is_object($category) && $category->parent !== 0);

	// $classes[] = $prefix . $category->slug;

	return $classes;
}

function farchioni_add_product_categories_to_body_class( $classes ) {
	if ( ! is_product() ) {
		return $classes;
	}
	$prefix = 'farchioni-category-';

	$customTerms = get_the_terms( 0, 'product_cat' );
	if ( is_array( $customTerms ) ) {
		foreach ( $customTerms as $customTerm ) {
			$classes[] = $prefix . $customTerm->slug;
		}
	}

	return $classes;
}
