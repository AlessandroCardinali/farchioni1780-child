<?php

/**
 * FUNZIONA
 */
function farchioni_cli_sync_product_data( $orig_id, $trnsl_id, $lang ) {
    global $woocommerce_wpml;
    $woocommerce_wpml->sync_product_data->sync_product_data( $orig_id, $trnsl_id, $lang );
}

/**
 * FUNZIONA
 */
function farchioni_cli_quick_edit_save_wpml( $post_id ) {
    global $woocommerce_wpml;
    $product = wc_get_product( $post_id );
    $woocommerce_wpml->sync_product_data->woocommerce_product_quick_edit_save( $product );
}

/**
 * Non funziona (probabilmente a causa dei controlli di WC)
 */
function farchioni_cli_synchronize_products( $post_id ) {
    global $woocommerce_wpml;
    $woocommerce_wpml->sync_product_data->synchronize_products( $post_id, get_post( $post_id ) );
}

/**
 * Non funziona (probabilmente a causa dei controlli di WC)
 */
function farchioni_cli_quick_edit_save( $post_id ) {
    $product = wc_get_product( $post_id );
    do_action( 'woocommerce_product_quick_edit_save', $product );
}

