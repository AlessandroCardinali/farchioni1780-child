��          �   %   �      P  /   Q  .   �  ,   �  E   �  �   #  F   �     '     /  �   I     �     �     �            L   +     x     �     �  
   �     �     �  +   �  -   !     O     a  �  f     4     Q  '   q  V   �  �   �  �   �	     q
     ~
  k   �
               *     ?     T  C   k     �     �     �  
   �            '   $  /   L     |     �                          	                           
                                                                        <strong>Error</strong>: First name is required! <strong>Error</strong>: Last name is required! <strong>Error</strong>: Privacy is required! <strong>Shipping Country</strong> must be the same as Billing Country <strong>Your request is now being processed by our staff</strong>; in the event that you do not hear from us in the next 48 hours, please contact us using the Help section of this website. An error occurred during the activation, please contact us for support Buy now Choose methods of payment Click on "Products" in the top menu to browse and buy Farchioni 1780 products; your dedicated B2B discount will be automatically applied. Coupon Code Download the label I'm a new user I'm already registered Invalid VAT Number Invalid VAT Number: please do not include the country code in the VAT number Login with your credentials. Please insert yout VAT Number Proceed to checkout Quantity:  Select your Country Taxes excluded Thank you for confirming your email address Your email address has already been confirmed each piece/bottle from Project-Id-Version: Farchioni1780 Child
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-09-27 17:26+0200
Last-Translator: 
Language-Team: Italiano
Language: it_IT
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 3.1.1
X-Loco-Version: 2.4.3; wp-5.4.2
X-Poedit-Basepath: ..
X-Poedit-Mapping: .php=gettext:PHP
X-Poedit-KeywordsList: _e;__
X-Poedit-SearchPath-0: .
 Si prega di inserire il nome Si prega di inserire il cognome Si prega di accettare la Privacy Policy <strong>Nazione di spedizione</strong> deve essere uguale alla nazione di fatturazione </strong>La sua richiesta di registrazione è ora in fase di approvazione</strong>; nel caso non dovesse ricevere risposta nelle prossime 48 la preghiamo di contattarci tramite la sezione Assistenza di questo sito. Si è verificato un errore inatteso durante la fase di attivazione del suo account, la preghiamo di contattare il nostro supporto tecnico tramite le pagine di Assistenza Acquista ora Scegli il metodo di pagamento Clicca su "Prodotti" nel menu per acquistare i prodotti di Farchioni 1780 con la scontistica a te dedicata. Codice Sconto Scarica l'etichetta Sono un Nuovo Utente Sono già registrato Partita Iva non valida Partita Iva non valida: si prega di non inserire il codice del pese Accedi con le tue credenziali. Inserire la partita Iva Procedi al Checkout Quantità: Seleziona il paese Iva esclusa Grazie per aver confermato la sua email Il suo indirizzo email è stato già confermato ogni pezzo/bottiglia da 