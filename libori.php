<?php

/**
 * LIBORI
 * EMAIL: Cambia reply-to per tutte le email di Woocommerce
 */
add_filter( 'woocommerce_email_headers', 'change_reply_to_email_address', 10, 3 );
function change_reply_to_email_address( $header, $email_id, $order ) {

		// HERE below set the name and the email address
		$reply_to_name  = 'Noreply';
		$reply_to_email = 'noreply@farchioni1780.com';

		// Get the WC_Email instance Object
		$email = new WC_Email($email_id);

		$header  = "Content-Type: " . $email->get_content_type() . "\r\n";
		$header .= 'Reply-to: ' . $reply_to_name . ' <' . $reply_to_email . ">\r\n";

		return $header;
}


/**
 * LIBORI
 * EMAIL: Cambia mittente per le email di wordpress
 */
// Function to change email address
function wpb_sender_email( $original_email_address ) {
		return 'store@farchioni1780.com';
}

// Function to change sender name
function wpb_sender_name( $original_email_from ) {
		return 'Store Farchioni1780';
}

// Hooking up our functions to WordPress filters
add_filter( 'wp_mail_from', 'wpb_sender_email' );
add_filter( 'wp_mail_from_name', 'wpb_sender_name' );


/**
 * LIBORI
 * Muovi in fondo alle pagine di listing i prodotti esauriti
 * @snippet     Order product collections by stock status, instock products first.
 * @author      Rkoms
 */

class iWC_Orderby_Stock_Status {

		public function __construct() {
				if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
						add_filter('posts_clauses', array($this, 'order_by_stock_status'), 2000, 2);
				}
		}

		public function order_by_stock_status($posts_clauses, $query) {
				global $wpdb;
				if ( $query->is_main_query() && is_woocommerce() && (is_shop() || is_product_category() || is_product_tag() || is_tax())) {
						$posts_clauses['join'] .= " INNER JOIN $wpdb->postmeta istockstatus ON ($wpdb->posts.ID = istockstatus.post_id) ";
						$posts_clauses['orderby'] = " istockstatus.meta_value ASC, " . $posts_clauses['orderby'];
						$posts_clauses['where'] = " AND istockstatus.meta_key = '_stock_status' AND istockstatus.meta_value <> '' " . $posts_clauses['where'];
				}
				return $posts_clauses;
		}

}
new iWC_Orderby_Stock_Status;


/**
 * LIBORI
 * NASCONDI I PRODOTTI OUTOFSTOCK DAI CONSIGLIATI
 */
// Hide out of stock related products in WooCommerce
add_filter( 'woocommerce_product_related_posts_query', 'alter_product_related_posts_query', 10, 3 );
function alter_product_related_posts_query( $query, $product_id, $args ){
		global $wpdb;

		$query['join']  .= " INNER JOIN {$wpdb->postmeta} as pm ON p.ID = pm.post_id ";
		$query['where'] .= " AND pm.meta_key = '_stock_status' AND meta_value = 'instock' ";

		return $query;
}
