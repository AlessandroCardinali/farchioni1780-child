<?php
/**
 * Single Product tabs
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/tabs.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see    https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.8.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

//Botta : richiamo la parte di template che gestisce i sentori e la descrizione
?>

<?php

/**
 * Indicatori vino e birra
 */
if ( is_countable( get_field( 'indicatori_select' ) ) ) {

	$cols = 'col-xs-12 col-sm-6';

	$count = count( get_field( 'indicatori_select' ) );

	global $product;
	$product_id = $product->get_id();

	switch ( $count ) {
		case 3:
			$cols = 'col-xs-12 col-sm-4';
			break;
		case 4:
			$cols = 'col-xs-12 col-sm-6 col-lg-3';
			break;
		case 0:
			$cols = '';
	}

	if ( have_rows( 'indicatori_select' ) ) {
		?>
	<div class="row" id="indicators_container">
		<?php
		while ( have_rows( 'indicatori_select' ) ) {
			the_row();
			?>

		<div class="<?php echo $cols; ?>">
			<div class="row meter_container">
				<div class="col-sm-12"><span class="indicator_title"><?php the_sub_field( 'nome_indicatore' ); ?></span></div>
				<div class="col-sm-12">
					<?php if ( has_term( 'vino', 'product_cat', $product_id ) ) { ?>
						<img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/vino/' . get_sub_field( 'valore_indicatore' ); ?>.png" />
					<?php } else { ?>
						<img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/birra/' . get_sub_field( 'valore_indicatore' ); ?>.png" />
					<?php } ?>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
		<?php
	}
}
?>


<?php

/**
 * Indicatori olio
 */
if ( is_countable( get_field( 'indicatori_select_olio' ) ) ) {

	$cols = 'col-xs-12 col-sm-6';

	$count = count( get_field( 'indicatori_select_olio' ) );

	switch ( $count ) {
		case 3:
			$cols = 'col-xs-12 col-sm-4';
			break;
		case 4:
			$cols = 'col-xs-12 col-sm-3';
			break;
		case 0:
			$cols = '';
	}

	if ( have_rows( 'indicatori_select_olio' ) ) {
		?>
	<div class="row" id="oil_indicators_container">
		<?php
		while ( have_rows( 'indicatori_select_olio' ) ) {
			the_row();
			?>
		<div class="<?php echo $cols; ?>">
			<div class="row meter_container">
				<div class="col-sm-12"><span class="indicator_title"><?php the_sub_field( 'nome_indicatore' ); ?></span></div>
				<div class="col-sm-12">
					<!--<img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/' . get_sub_field( 'valore_indicatore' ); ?>_olio.png" /> -->
					<?php
					// Mostro i rettangoli pieni
					for ( $i = 0;$i < (int) get_sub_field( 'valore_indicatore' );$i++ ) {
						?>
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/olio/rettangolo_pieno.png" />
						<?php
					}
					?>

					<?php
					// Mostro i rettangoli vuoti
					for ( $i = (int) get_sub_field( 'valore_indicatore' );$i < 10;$i++ ) {
						?>
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/olio/rettangolo_vuoto.png" />
						<?php
					}
					?>
				</div>
			</div>
			<div class="row meter_container_ext">
				<div class="col-sm-12 under_oil_indicators">
					<?php
					// Mostro la vignetta sotto l'indicatore
					// Parto dal presupposto che l'indicatore sia "leggero"
					$indicatore = 'light';

					if ( (int) get_sub_field( 'valore_indicatore' ) >= 4 && (int) get_sub_field( 'valore_indicatore' ) <= 6 ) {
						$indicatore = 'medium';
					} else {
						if ( (int) get_sub_field( 'valore_indicatore' ) >= 7 ) {
							$indicatore = 'intense';
						}
					}

					$indicatore_tradotto = $indicatore;

					switch ( ICL_LANGUAGE_CODE ) {
						case 'it':
						{
							switch ( $indicatore ) {
								case 'light':
								{
									$indicatore_tradotto = 'leggero';
								}break;

								case 'medium':
								{
									$indicatore_tradotto = 'medio';
								}break;

								case 'intense':
								{
									$indicatore_tradotto = 'intenso';
								}break;
							}
						}break;
					}

					?>
					<div class="indicator_container <?php echo $indicatore; ?>">
						<img
							src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/olio/<?php echo $indicatore; ?>.png" />
						<div class="centered"><?php echo _e( $indicatore_tradotto, 'doma' ); ?></div>
					</div>
					<style type="text/css">
						.meter_container {
							max-width:100%;
							margin: 0 auto;
							overflow: hidden;
						}
						@media (max-width: 767px){
							.meter_container {
								min-width: 100%;
							}
						}
						.meter_container img{
							max-width:8.5%;
							height:11px;
							margin-right:1.5%;
							float: left;
						}
						.meter_container img:last-child{
							margin-right:0;
						}
						.meter_container_ext {
							width: 100%;
							max-width:100%;
							margin: 0 auto;
						}

						.under_oil_indicators {
							padding-left: 0;
							padding-right: 0;
							padding-top: 5px;
						}

						.indicator_container {
							width: 100px;
							height: 35px;
							position: relative;
						}

						.indicator_container img {
							position: absolute;
							top: 0;
							left: 0;
							z-index: 1;
						}

						.indicator_container .centered {
							height: 35px;
							width: 100px;
							padding-top: 5px;
							line-height: 28px;
							font-size: 17px;
							text-align: center;
							text-transform: capitalize;
							color: #104633;
							font-style: italic;
							z-index: 2;
							position: absolute;
							top: 0;
							left: 0;
							font-family: 'playfair_displayregular';
						}

						#oil_indicators_container {
							padding-bottom: 60px;
						}

						.indicator_container.medium {
							margin: 0 auto;
						}

						.intense {
							margin-left: 160px;
						}

						.light {}

					</style>
				</div>
			</div>
		</div>
			<?php
		}
		?>
	</div>

		<?php
	}
}
?>

<?php

/**
 * Immagine descrittiva o Video + descrizione prodotto
 */
$immagine_desc_id = get_post_meta( get_the_ID(), 'immagine_desc', true );

if ( $immagine_desc_id ) {
	$img = wp_get_attachment_image_url($immagine_desc_id, 'medium_large');
} else {
	$img = '';
}
if ( get_field( 'video_desc' ) ) {
	$video = get_field( 'video_desc' );
} else {
	$video = '';
}
?>
<div class="row" id="description_container">
	<?php if ( $img != '' || $video != '' ) { ?>
		<div class="col-xs-12 col-sm-6" id="img_description_container">
			<?php if ( $video ){ ?>
				<div class="embed-container"><?php echo $video; ?></div>
			<?php } else { ?>
				<img class="img-fluid" src="<?php echo $img; ?>" />
			<?php } ?>

		</div>
		<div class="col-xs-12 col-sm-6" id="txt_description_container">
	<?php } else { ?>
		<div class="col-xs-12 col-sm-12" id="txt_description_container">
	<?php } ?>

		<div class="paragraphs_container">
			<div class="col-sm-12">
				<div ><!--FT commento class="row"-->
					<?php
					//$descrizione = get_post_meta(get_the_ID(), 'campo_descrizione_lunga', true);
					// FT: sostituito la precendente con la successiva per avere filtro autop sul contenuto testuale.
					$descrizione = get_field('campo_descrizione_lunga');

						if ($descrizione) {
							echo $descrizione;
						}
					?>
				</div>
				<?php

					$is_b2b_user = is_b2b_user();

					$pdf_etichetta_id = get_post_meta( get_the_ID(), 'pdf_etichetta', true );
					if ( $pdf_etichetta_id && !$is_b2b_user ) {
					?>
				<div class="farchioni-button">
				<div class="elementor-cta__button-wrapper elementor-cta__content-item elementor-content-item ">
					<a href="<?php echo wp_get_attachment_url($pdf_etichetta_id); ?>" class="elementor-cta__button elementor-button elementor-size-md pdf-button"
						target="_blank"><?php _e( 'Download the label', 'farchioni1780-child'); ?></a>
				</div></div>
						<?php
					}

				$pdf_scheda_id = get_post_meta( get_the_ID(), 'pdf_scheda', true );
				if ( $pdf_scheda_id && $is_b2b_user ) { ?>
					<div class="farchioni-button">
						<div class="elementor-cta__button-wrapper elementor-cta__content-item elementor-content-item ">
							<a href="<?php echo wp_get_attachment_url($pdf_scheda_id); ?>" class="elementor-cta__button elementor-button elementor-size-md pdf-button"
										target="_blank"><?php _e( 'Download technical sheet', 'farchioni1780-child'); ?></a>
						</div>
					</div>
				<?php } ?>

			</div>
		</div>
	</div>

	<?php if ( function_exists( 'ntav_netreviews_parse_request' ) ) : // recensioni verificate ?>
	<div class="farchioni-reviews-tab col-xs-12 col-sm-12" id="tab-reviews">
		<div id="txt_netreviews_container"> <?php ntav_netreviews_tab(); ?> </div>
	</div>
	<?php endif; ?>

	<?php


	/**
	 * Filter tabs and allow third parties to add their own.
	 *
	 * Each tab is an array containing title, callback and priority.
	 * @see woocommerce_default_product_tabs()
	 */
	do_action( 'zoo_custom_html_inner_after' );
	do_action( 'zoo_custom_html_wrap_after' );
	$tabs = apply_filters( 'woocommerce_product_tabs', array() );
	if ( zoo_woo_enable_share() ) {
		do_action( 'zoo_woocommerce_template_single_sharing' );
	}
	$tabs_class = '';
	//Change layout tab to accordion follow layout
	if ( zoo_woo_gallery_layout_single() == 'sticky-accordion' || zoo_woo_gallery_layout_single() == 'images-center' ) {
		$tabs_class .= ' zoo-accordion';
	} else {
		$tabs_class .= ' wc-tabs-wrapper';
	}
	if ( ! empty( $tabs ) ) :
		?>


	<div class="zoo-woo-tabs <?php echo ent2ncr( $tabs_class ); ?>">
			<?php
			if ( zoo_woo_gallery_layout_single() != 'sticky-accordion' && zoo_woo_gallery_layout_single() != 'images-center' ) {
				?>
		<ul class="tabs wc-tabs zoo-tabs">
				<?php foreach ( $tabs as $key => $tab ) : ?>
			<li class="<?php echo esc_attr( $key ); ?>_tab">
				<a
					href="#tab-<?php echo esc_attr( $key ); ?>"><?php echo apply_filters( 'woocommerce_product_' . $key . '_tab_title', esc_html( $tab['title'] ), $key ); ?></a>
			</li>
			<?php endforeach; ?>
		</ul>
			<?php } ?>
		<div class="tab-content">
			<?php
			foreach ( $tabs as $key => $tab ) :
				if ( zoo_woo_gallery_layout_single() == 'sticky-accordion' || zoo_woo_gallery_layout_single() == 'images-center' ) {
					?>
			<div class="zoo-group-accordion">
				<h3 class="tab-heading">
					<?php echo apply_filters( 'woocommerce_product_' . $key . '_tab_title', esc_html( $tab['title'] ), $key ); ?>
					<i class="cs-font clever-icon-down"></i></h3>
				<?php } ?>
				<div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--<?php echo esc_attr( $key ); ?> panel entry-content wc-tab"
					id="tab-<?php echo esc_attr( $key ); ?>">
					<h3 class="tab-heading">
						<?php echo apply_filters( 'woocommerce_product_' . $key . '_tab_title', esc_html( $tab['title'] ), $key ); ?>
					</h3>
					<?php call_user_func( $tab['callback'], $key, $tab ); ?>
				</div>
				<?php
				if ( zoo_woo_gallery_layout_single() == 'sticky-accordion' || zoo_woo_gallery_layout_single() == 'images-center' ) {
					?>
			</div>
			<?php } ?>
			<?php endforeach; ?>
		</div>
	</div>
		<?php endif; ?>
</div>
