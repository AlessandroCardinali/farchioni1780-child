<?php
/**
 * Template Order step
 * @since: zoo-theme 1.0
 */
?>
<div class="row">
	<div class="col-sm-12" id="phase_container">
		<img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/phase_1.png'; ?>" id="phase_1" />
		<img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/phase_2.png'; ?>" id="phase_2" />
		<img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/phase_3.png'; ?>" id="phase_3" />
	</div>
</div>
<div id="order-step">
	<?php if ( is_cart() ) { ?>
		<?php esc_html_e( 'Shopping Cart', 'doma' ); ?>
	<?php } elseif ( is_checkout() && ! is_order_received_page() ) { ?>
		<?php esc_html_e( 'Check Out Detail', 'doma' ); ?>
	<?php } elseif ( is_order_received_page() ) { ?>
		<?php esc_html_e( 'Order awaiting processing', 'doma' ); } ?>
</div>
