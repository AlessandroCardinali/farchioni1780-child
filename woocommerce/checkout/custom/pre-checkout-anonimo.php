<style type="text/css">
	#checkout_container{ display: none; }
	#phase_1, #phase_3{ display: none; }
	#phase_2{ display: inline-block; }
</style>

<div class="mb-4 d-flex justify-content-center">
<?php echo do_shortcode('[nextend_social_login]'); ?>
</div>

<div class="row" id="precheckout_wrapper">
	<div class="col col-sm-6 col-xs-12 login">
		<div class="row">
			<div class="col-sm-12">
				<h3 class="precheckout_title"><?php echo _e( "I'm already registered", 'farchioni1780-child' ); ?></h3>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<p class="choice-title"><?php echo _e( 'Login with your credentials.', 'farchioni1780-child' ); ?></p>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
			<?php
			$args['remember'] = false;
			wp_login_form( $args ); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<!-- <a id="password_recover" href="<?php // echo wp_lostpassword_url( $redirect ); ?>"><?php // echo _e( 'Recover your password', 'farchioni1780-child' ); ?></a> -->

				<?php // do_action('oa_social_login'); ?>
			</div>

		</div>
	</div>
	<div class="col col-sm-6 col-xs-12 guest px-5">
		<div class="row">
			<div class="col-sm-12">
				<h3 class="precheckout_title"><?php echo _e( "I'm a new user", 'farchioni1780-child' ); ?></h3>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<p class="precheckout_text precheckout_text_btn">
					<a href="javascript:void(0);" onclick="hide_precheckout(false);" id="hide_precheckout"><?php echo _e( 'Proceed to checkout', 'farchioni1780-child' ); ?></a>
				</p>
				<p class="choice-title"><?php _e('Se lo desideri, puoi registrarti durante il checkout per effettuare più velocemente i prossimi acquisti','farchioni-child');?>.</p>

				<!-- If you want you can register now to speed up your future orders  -->
				<p class="precheckout_text precheckout_text_btn">
					<a href="javascript:void(0);" onclick="hide_precheckout(true);" ><?php _e('Crea account','farchioni-child');?></a>
				</p>

				<div class="precheckout-listino-b2b">
					<p><strong><?php _e( 'Are you a business?','farchioni1780-child');?></strong><br/>
					<a href="<?php echo get_page_link(118902);?>"><?php echo _e( 'Richiedi un account per la tua azienda per accedere al listino dedicato', 'farchioni1780-child' ); ?></a>
					</p>
				</div>

			</div>
		</div>
	</div>
</div>
