<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     3.5.0
 */


 /**
 *  note da idearia a fosforica
 *
 *	myfield13_field CF
 *	myfield8_field IVA
 *	myfield15_field Pec o SDI
 *
 *
 */


if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

get_template_part( 'woocommerce/theme-custom/order', 'step' );

?>
<div class="wrap-checkout-notices">
	<?php wc_print_notices(); ?>
</div>
<?php
// in questa azione normalmente si aggancerebbe il modulo a scomparsa per i coupon (e chissa cos'altro), ma Idearia l'ha segato
// do_action( 'woocommerce_before_checkout_form', $checkout );

if ( ! is_user_logged_in() ) {
	// contiene i due box di login oppure iscrizione.
	include('custom/pre-checkout-anonimo.php');
} else {
	// solo css
	include('custom/pre-checkout-logged.php');
}
?>
<script type="text/javascript">
	function hide_precheckout( create_account = false ){
		try{
			document.getElementById("precheckout_wrapper").style.display="none";
		} catch (ex) {

		}

		if (create_account === true){
			setTimeout( function(){
				document.getElementById('createaccount').click();
			}, 2000);
		}

		document.getElementById("checkout_container").style.display="block";
		document.getElementById("phase_1").style.display = "none";
		document.getElementById("phase_2").style.display = "none";
		document.getElementById("phase_3").style.display = "inline-block";
	}
</script>
<?php
//Nascondo il precheckout se un coupon è stato aggiunto
if ( isset( $_GET['coupon'] ) ) {
	echo '<script type="text/javascript">hide_precheckout();</script>';
}
?>
<div id="checkout_container">
	<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">
		<div class="row">
			<div class="col-xs-12 col-sm-7">
				<?php if ( $checkout->get_checkout_fields() ) : ?>

					<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

					<div class="col2-set" id="customer_details">
						<?php do_action( 'woocommerce_checkout_billing' ); ?>
						<?php do_action( 'woocommerce_checkout_shipping' ); ?>
					</div>

					<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

				<?php endif; ?>
			</div>
			<div class="col-xs-12 col-sm-5">

				<div id="order_review" class="woocommerce-checkout-review-order">
					<?php do_action( 'woocommerce_checkout_before_order_review_heading' ); ?>
					<h3 class="checkout-block-title"><?php _e( 'Your order', 'woocommerce' ); ?></h3>
					<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>
					<?php do_action( 'woocommerce_checkout_order_review' ); ?>
				</div>

				<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>
			</div>
		</div>
	</form>

<style type="text/css">
	#billing_myfield13_field, #billing_company_field, #billing_myfield8_field, #billing_myfield15_field{
		display: none;
	}
</style>

<script type="text/javascript">
	// se utente B2B metto tutto in readonly
	<?php if ( is_b2b_user() ){ ?>
		jQuery("#billing_myfield12").attr('readonly',true).on('click',function(){
			return false;
		})
// 		jQuery("#billing_myfield8").attr('readonly',true);
// 		jQuery("#billing_company").attr('readonly',true);
//



	<?php
	} else {  // JS per tutti gli altri
	?>

	document.getElementById("billing_country").onchange = function(){
		console.log('la nazione cambia');

		var e = document.getElementById("billing_country");
		var selectedCountry = e.options[e.selectedIndex].value;
		var checkbox_field = document.getElementById("billing_myfield12");

		if (selectedCountry == 'IT' && checkbox_field.checked === true)
		{
			document.getElementById("billing_myfield15").readOnly = false;
			document.getElementById("billing_myfield15").value = "";
			document.getElementById("billing_myfield15_field").style.display = "block";
		}
		else
		{
			document.getElementById("billing_myfield15").value = "-";
			document.getElementById("billing_myfield15").readOnly = true;
			document.getElementById("billing_myfield15_field").style.display = "none";
		}
	}


	document.getElementById("billing_myfield12").onchange = function(){
		var e = document.getElementById("billing_country");
		var selectedCountry = e.options[e.selectedIndex].value;
		if (document.getElementById("billing_myfield12").checked){

			document.getElementById("billing_myfield13_field").style.display="block";
			document.getElementById("billing_company_field").style.display="block";
			document.getElementById("billing_myfield8_field").style.display="block";
			document.getElementById("billing_myfield15_field").style.display="block";

			document.getElementById("billing_myfield13").value = "";
			document.getElementById("billing_company").value = "";
			document.getElementById("billing_myfield8").value = "";
			if (selectedCountry == 'IT'){
				document.getElementById("billing_myfield15").readOnly = false;
				document.getElementById("billing_myfield15").value = "";
				document.getElementById("billing_myfield15_field").style.display = "block";
			} else {
				document.getElementById("billing_myfield15").value = "-";
				document.getElementById("billing_myfield15").readOnly = true;
				document.getElementById("billing_myfield15_field").style.display = "none";
			}

		} else {

			document.getElementById("billing_myfield15").readOnly = false;

			document.getElementById("billing_myfield13_field").style.display="none";
			document.getElementById("billing_company_field").style.display="none";
			document.getElementById("billing_myfield8_field").style.display="none";
			document.getElementById("billing_myfield15_field").style.display="none";

			document.getElementById("billing_myfield13").value = "-";
			document.getElementById("billing_company").value = "-";
			document.getElementById("billing_myfield8").value = "-";
			document.getElementById("billing_myfield15").value = "-";
		}
	}

	/*
	document.getElementById("billing_myfield8").blur = function()
	{
		jQuery("#billing_postcode").change();
	};
	*/
	jQuery(document).ready(function(){

		//Setto il paese di spedizione come readonly
		var mask = document.createElement("div");
		jQuery(mask).addClass("full_element_wrapper");
		//jQuery(mask).css("height","50px").css("z-index","999999").css("position","absolute").css(;
		jQuery("#shipping_country_field").css("position","relative");
		jQuery("#shipping_country_field").append(mask);

		// L'inizializzazione con il trattino serve ad evitare che
		// WooCommerce blocchi il checkout a causa della flag required
		// ad es. nel caso di privati che acquistano e quindi lasciano
		// non compilati i campi (per loro invisibili) di SDI, partita
		// IVA, codice fiscale e azienda.
		document.getElementById("billing_myfield13").value = "-";
		document.getElementById("billing_company").value = "-";
		document.getElementById("billing_myfield8").value = "-";
		document.getElementById("billing_myfield15").value = "-";
		var e = document.getElementById("billing_country");
		var selectedCountry = e.options[e.selectedIndex].value;


		// FT if (document.getElementById("billing_myfield12_checkbox").checked)
		if (document.getElementById("billing_myfield12").checked){
			document.getElementById("billing_myfield13_field").style.display="block";
			document.getElementById("billing_company_field").style.display="block";
			document.getElementById("billing_myfield8_field").style.display="block";
			document.getElementById("billing_myfield15_field").style.display="block";

			if (selectedCountry == 'IT'){
				document.getElementById("billing_myfield15").readOnly = false;
				document.getElementById("billing_myfield15").value = "";
				document.getElementById("billing_myfield15_field").style.display = "block";
			} else {
				document.getElementById("billing_myfield15").value = "-";
				document.getElementById("billing_myfield15").readOnly = true;
				document.getElementById("billing_myfield15_field").style.display = "none";
			}
		} else {
			document.getElementById("billing_myfield13_field").style.display="none";
			document.getElementById("billing_company_field").style.display="none";
			document.getElementById("billing_myfield8_field").style.display="none";
			document.getElementById("billing_myfield15_field").style.display="none";
			document.getElementById("billing_myfield15").readOnly = false;
		}

		//Controllo se il box con il flag della partita iva è checkato o meno: in caso positivo, mostro il riquadro con i dati fiscali aziendali

		//Scateno il controllo sulla partita iva al cambiare del valore della partita iva
		jQuery('#billing_myfield8').change(function () {
			var billing_vat = jQuery('#billing_myfield8').val();
			var billing_country = jQuery('#billing_country').val();

			if (billing_country.length > 0 && billing_vat.length){
				//jQuery( 'body' ).trigger( 'update_checkout' );
				//console.log(billing_vat + " - " + billing_country);
				jQuery( 'body' ).trigger( 'update_checkout' );
		 	}
			return false;
		});


	//Svuotare i campi di billing e shipping al cambiare del billing country e poi sincronizzare i billing e shipping countries.
	var isFirstLoad = true;
	var isVatModified = false;

	jQuery("#billing_country").change(function(){

		isFirstLoad = false;
		//Svuoto tutti i campi di billing
		var toemptybilling = new Array();
		toemptybilling.push("billing_address_1");
		toemptybilling.push("billing_address_2");
		toemptybilling.push("billing_postcode");
		toemptybilling.push("billing_city");
		toemptybilling.push("billing_state");

		emptyElements(toemptybilling);

		var toempty = new Array();
		toempty.push("shipping_address_1");
		toempty.push("shipping_address_2");
		toempty.push("shipping_postcode");
		toempty.push("shipping_city");
		toempty.push("shipping_state");
		//Se il campo di spedizione ad indirizzo diverso è flaggato, sincronizzo i due campi country

		//Svuotare i campi di shipping relativi all'indirizzo
		emptyElements(toempty);

		var tmp = jQuery("#billing_country").val();
		console.log(tmp);
		//jQuery("#shipping_country").val(jQuery("#billing_country").val());
		jQuery("#shipping_country option").removeAttr('selected');
		jQuery("#shipping_country option[value='" + tmp +"']").attr('selected','selected');

		setTimeout(function(){ jQuery("#shipping_country").trigger("change"); }, 500);

		setTimeout(function(){ jQuery( 'body' ).trigger( 'update_checkout' ); }, 1000);
		console.log("reloaded");
		//setTimeout(function(){ jQuery("#shipping_country_field *").unbind(); }, 1500);
		//jQuery("#billing_country").trigger("change");
		//jQuery( 'body' ).trigger( 'update_checkout' );

	});

	jQuery("#billing_myfield8").change(function(){
		isVatModified = true;
	});

	jQuery("#billing_myfield8").blur(function(){
		if (!isFirstLoad && isVatModified){
			setTimeout(function(){ jQuery( 'body' ).trigger( 'update_checkout' ); }, 500);
		}
		isVatModified = false;
	});


	//Modifico la struttura del flag di inserimento nella newsletter
	var sendinbluetext = jQuery("#ws_opt_in_field label").text();
	var totalText = sendinbluetext;
	var tmp = sendinbluetext.split("(");
	sendinbluetext = tmp[0].trim();

<?php if ( ICL_LANGUAGE_CODE == 'it' ) { ?>
		sendinbluetext = "Iscriviti alla newsletter di Farchioni";
<?php } ?>

	var newElement = jQuery("<span>" + sendinbluetext + " " + jQuery("#ws_opt_in_field label .optional").text() + "</span>");
	var new2 = jQuery("<label></label>");
	jQuery("#ws_opt_in").appendTo(jQuery(new2));
	//jQuery("#ws_opt_in_field label").appendTo(newElement);
	jQuery(newElement).appendTo(new2);
	jQuery(new2).appendTo(jQuery("#ws_opt_in_field"));

	jQuery("#ws_opt_in_field .woocommerce-input-wrapper").remove();

});

	function emptyElements(elementsList){
		for (var i=0;i<elementsList.length;i++){
			try{
				jQuery("#" + elementsList[i]).val("");
			} catch(ex)	{

			}
		}
	}

	var checkout_form = jQuery( 'form#checkout' );
	checkout_form.on( 'checkout_place_order', function(){
		jQuery("#billing_country").change();
		return true;
	});
<?php } ?>
</script>
<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
</div>
