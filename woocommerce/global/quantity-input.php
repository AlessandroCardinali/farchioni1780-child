<?php
/**
 * Product quantity inputs
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/quantity-input.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     4.0.0
 */

// NOTA 2021 10 11 ELIMINARE b2b_unit

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
global $product;

if ( $max_value && $min_value === $max_value ) {
?>
<div class="quantity hidden">
	<input type="hidden" id="<?php echo esc_attr( $input_id ); ?>" class="qty" name="<?php echo esc_attr( $input_name ); ?>" value="<?php echo esc_attr( $min_value ); ?>" />
</div>
<?php } else { ?>
<div class="price_info">
	<?php
		if ( is_product() ){

			$label = ! empty( $args['product_name'] ) ? sprintf( esc_html__( '%s quantity', 'woocommerce' ), wp_strip_all_tags( $args['product_name'] ) ) : esc_html__( 'Quantity', 'woocommerce' );

	?>
	<div class="quantity">
		<?php do_action( 'woocommerce_before_quantity_input_field' ); ?>
		<div class="header"><?php _e("Quantity","doma-child"); ?></div>
		<div class="body">
			<span class="qty-nav decrease">-</span>
			<label class="screen-reader-text" for="<?php echo esc_attr( $input_id ); ?>"><?php echo esc_attr( $label ); ?></label>
			<input
			type="number"
			id="<?php echo esc_attr( $input_id ); ?>"
			class="<?php echo esc_attr( join( ' ', (array) $classes ) ); ?>"
			step="<?php echo esc_attr( $step ); ?>"
			min="<?php echo esc_attr( $min_value ); ?>"
			max="<?php echo esc_attr( 0 < $max_value ? $max_value : '' ); ?>"
			name="<?php echo esc_attr( $input_name ); ?>"
			value="<?php echo esc_attr( $input_value ); ?>"
			title="<?php echo esc_attr_x( 'Qty', 'Product quantity input tooltip', 'woocommerce' ); ?>"
			size="4"
			placeholder="<?php echo esc_attr( $placeholder ); ?>"
			inputmode="<?php echo esc_attr( $inputmode ); ?>" />
			<span class="qty-nav increase">+</span>
		</div>
		<?php do_action( 'woocommerce_after_quantity_input_field' ); ?>
	</div>
	<?php
			//Testo se il prodotto ha un grado alcolico
			$grado = get_field( "grado_alcolico" );
			if ($grado){
	?>
	<div class="grado_alcolico">
		<div class="header"><?php _e("Alcohol volume","traduzioni_tema"); ?></div>
		<div class="body">
			<p>
			<?php
				//Modifica Botta: ottengo il grado alcoolico del prodotto attuale, se è impostato
				echo get_field("grado_alcolico");
				_e("% Vol","traduzioni_tema");
			?>
			</p>
		</div>

	</div>
	<?php
			} // fine grado
	?>
	<div class="formato" id="variation_destination">
		<?php
			// Modifica Botta
			// global $product;

			// $product->is_type( $type ) checks the product type, string/array $type ( 'simple', 'grouped', 'variable', 'external' ), returns boolean

			if ( $product->is_type( 'variable' ) ){ ?>
			<div class="center_variation_choose header"><strong><?php _e("Choose a format","doma-child"); ?></strong></div>
			<div class="variation_selection">
			<?php
				$products = get_all_variations_data($product->get_id());
				$is_b2b_user = is_b2b_user();
				foreach($products as $prod){
					// $b2b_units = '';
					// if ( $is_b2b_user ){
					// 	$b2b_units = ($unit = get_field('b2b_units',$prod['variation_id']) )? 'data-b2b_units="'.$unit.'"' : '';
					// }

					$class = $prod["formato"];
					if ($prod['is_default'] === "1"){ $class.=" selected"; }

					$print = "";
					if ($prod["display_price"] !== $prod["display_regular_price"]){
						$print = "&nbsp;-" . (100 - round(($prod["display_price"] * 100)/$prod["display_regular_price"],0)) . "%";
					}

					$ispromo=(strlen($print)>0) ? "is_promo" : "";  ?>
				<div class="variation_container row <?php echo $class; ?> <?php echo $ispromo; ?>" data-variation-reference="<?php echo $prod["formato"]; ?>" <?php // echo $b2b_units;?> >
					<div class="variation_sale_icon variation_attribute col-xs-2"><img src="<?php echo get_stylesheet_directory_uri() . "/assets/img/sale.png" ?>" class="sale_icon_variation" /></div>
					<div class="variation_promo variation_attribute col-xs-4"><?php echo $print; ?> </div>
					<div class="variation_formato variation_attribute col-xs-6"><?php echo $prod["name"]; ?></div>
				</div>
		<?php } // fine foreach $products	?>
				</div>
		<?php } // fine se product variable ?>
	</div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function()
	{
		//Inverto il formato con la quantità
		jQuery(".price_info").prepend(jQuery("#variation_destination"));

		//Rimuovo i rimasugli di selezione del formato
		jQuery(".summary.entry-summary.wrap-right-single-product > p.price").remove();

		//Imposto i click sulle variazioni
		jQuery(".variation_container").click(function(e)
		{
			var variation = jQuery(this).attr("data-variation-reference");
			// console.log(variation);

			jQuery("select[name=attribute_pa_formato]").val(variation).change();

			//Cambio il colore dei bottoni
			jQuery(".variation_container").removeClass("selected");
			jQuery(this).addClass("selected");
		});

		//fosforica 20211005
		//se non c'è un default clicco il primo
		if( jQuery('.variation_container.selected').length == 0 ){
			jQuery('.variation_selection>div:first-child').click();
		}
		// se ho in url parametro clicco la variazione
		let searchParams = new URLSearchParams(window.location.search)
		if ( searchParams.has('attribute_pa_formato')){
			jQuery('[data-variation-reference="'+searchParams.get('attribute_pa_formato')+'"]').click();
		}

	});
</script>
<br class="clear" />
	<?php
		} else {
	?>
	<div class="quantity">
		<?php do_action( 'woocommerce_before_quantity_input_field' ); ?>
		<label class="screen-reader-text" for="<?php echo esc_attr( $input_id ); ?>"><?php echo esc_attr( $label ); ?></label>
		<span class="qty-nav decrease">-</span>
		<input
			type="number"
			id="<?php echo esc_attr( $input_id ); ?>"
			class="<?php echo esc_attr( join( ' ', (array) $classes ) ); ?>"
			step="<?php echo esc_attr( $step ); ?>"
			min="<?php echo esc_attr( $min_value ); ?>"
			max="<?php echo esc_attr( 0 < $max_value ? $max_value : '' ); ?>"
			name="<?php echo esc_attr( $input_name ); ?>"
			value="<?php echo esc_attr( $input_value ); ?>"
			title="<?php echo esc_attr_x( 'Qty', 'Product quantity input tooltip', 'woocommerce' ); ?>"
			size="4"
			placeholder="<?php echo esc_attr( $placeholder ); ?>"
			inputmode="<?php echo esc_attr( $inputmode ); ?>" />
		<span class="qty-nav increase">+</span>
		<?php do_action( 'woocommerce_after_quantity_input_field' ); ?>
	</div>
	<?php
		}
}
