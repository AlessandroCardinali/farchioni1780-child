jQuery(document).ready(function($) {
    "use strict";

    /**
     * Edit labels id
     */
    var privacyPolicyLabelId = 'afreg_additional_18481';
    var newsLetterLabelId = 'afreg_additional_18482';

    /**
     * Custom code - stop edit
     * Unless you know what you're doing
     */

    // Get current language
    let currentLang = $('body').attr('class');
    currentLang = currentLang.search('current_lang_it');
    
    // -1 means the current_lang_it was not found in the body class
    if (currentLang === -1) {
        translateCheckbox();
    } else {
        addUrlToLabels();
    }


/**
 * Traduci in inglese le due checkbox usate durante la creazione dell'account
 */
function translateCheckbox() {    
    $('label[for="'+privacyPolicyLabelId+'"]').html('I read and understood the <a href="https://store.farchioni1780.com/en/privacy-policy/">Privacy</a> and <a href="https://store.farchioni1780.com/en/cookies/">cookies</a> policy');
    $('label[for="'+newsLetterLabelId+'"]').html('I read and understood the <a href="https://store.farchioni1780.com/en/privacy-policy/">Commercial</a> policy');
}

/**
 * Converti le url in html nelle due label in italiano
 */
function addUrlToLabels() {
    // TODO
}


});