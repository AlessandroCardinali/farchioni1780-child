// jQuery(document).ready(function ($) {
//     "use strict";

//     let $shippingCountry = $('#shipping_country');
//     let $billingCountry = $('#billing_country');
//     $shippingCountry.attr('disabled', 'disabled');

//     // Shipping e Billing Country devono avere lo stesso valore
//     updateShippingCountrySelect($shippingCountry, $billingCountry);

//     /**
//      * Aggiorna il campo nazione di spedizione con il valore del campo nazione di fatturazione
//      * @param {*} $shippingCountry 
//      * @param {*} $billingCountry 
//      */
//     function updateShippingCountrySelect($shippingCountry, $billingCountry) {
//         $billingCountry.on('change', function(e) {
//             // Valore select utilizzato da WC
//             $shippingCountry.val($billingCountry.val());
//             // Valore frontend utilizzato dalla select dei paesi
//             $('#select2-shipping_country-container').text($('#shipping_country :selected').text());

//             $('body').trigger( 'update_checkout' );
//         });
//     }
// });