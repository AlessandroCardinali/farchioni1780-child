jQuery(document).ready(function ($) {
	"use strict";

	toggleSidebar();

	/**
	 * Sidebar toggle
	 */
	function toggleSidebar() {
		// Remove body class
		$('body').on('touchstart click', cancelDuplicates(function (e) {
			if ($(e.target).hasClass('sidebar-mobile-toggle-active')) {
				$('body').removeClass('sidebar-mobile-toggle-active');
			}
		}));

		// Add body class
		$('.open-mobile-sidebar').on('touchstart click', cancelDuplicates(function () {
			$('body').addClass('sidebar-mobile-toggle-active');
		}));
	}


	/**
	 * Cancel duplicates functions within 10ms
	 * https://stackoverflow.com/questions/25572070/javascript-touchend-versus-click-dilemma
	 */
	function cancelDuplicates(fn, threshhold, scope) {
		if (typeof threshhold !== 'number') threshhold = 300;
		var last = 0;

		return function () {
			var now = +new Date;

			if (now >= last + threshhold) {
				last = now;
				fn.apply(scope || this, arguments);
			}
		};
	}

});
