jQuery(document).ready(function($) {
    "use strict";
   

    /**
     * La percentuale di sconto è inserita quando si seleziona una variazione di prodotto e al caricamento della pagina (on change)
     */
    add_product_discount_percentage();
    $( '.variations_form' ).each( function() {
        $(this).on( 'change', '.variations select', function() {
            add_product_discount_percentage();
        });
    });

    // Make selected the chosen variation on page load!
    show_selected_option_on_load();

/**
 * Aggiungi la percentuale di sconto al badge denntro la scheda prodotto
 */
function add_product_discount_percentage() {
    // Se non aspettiamo, il prezzo letto è quello della variazione precedente al update. 
    // (Stranamente nel sito vecchio funzionava bene senza questa regola)
    setTimeout(function(){
        let price = $('.woocommerce-variation-price .amount').text().replace(/ /g,'');
        let percent = null;
        price = price.replace(/,/g,".");
        let tmp = price.split("€");

        //Prodotto in sconto
        if (tmp.length == 3) {
            percent = (100 - (100 * tmp[2]) / tmp[1]).toFixed(1);
            percent = percent.replace(".0","");
        }

        // Se non c'è uno sconto sulla variazione attuale, nascondi la label
        if (percent != null) {
            percent = Math.floor(percent);
            $(".single .product .onsale").text("PROMO -" + percent + "%");
            $(".single .product .onsale").show();
        } else {
            $(".single .product .onsale").hide();
        }

    }, 300);
}

/**
 * Per evitare questo bug: https://idearia.link/OjX3YB
 * Quando apri la pagina di un prodotto sulla sua variazione (non principale),
 *  tutti i dati sono quelli del prodotto selezionato ma in "Scegli Formato"
 *  vedi selezionata la variazione di default. 
 *  Questa funzione serve a fare il fix di questo bug.
 * Prende il valore dalla select originale di woocommerce dove la variazione
 *  è impostata correttamente e la rispecchia sulla select visibile.
 */
function show_selected_option_on_load() {
    let selectedVariation = $('#pa_formato option:selected').val();
    let variationSection = $('.variation_selection .variation_container');

    variationSection.each(function() {
        let element = $(this);

        if (! element.hasClass(selectedVariation)) {
            element.removeClass('selected');
        }

        if (element.hasClass(selectedVariation) && ! element.hasClass('selected')) {
            element.addClass('selected');
        }

    });
}

});