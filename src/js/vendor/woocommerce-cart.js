jQuery(document).ready(function($) {
    "use strict";
    
    //Coupon code for cart page
    farchioni_add_coupon_cart();

    /**
     * La funzione sovrascrive la funzione di default che si trova in zoo-woocommerce.js
     *  chiamata zoo_add_coupon_cart(); la funzione ha richiesto la customizzazione delle classi utilizzate
     *  per rispecchiare il template del tema vecchio.
     */
    function farchioni_add_coupon_cart() {
        $(document).on('click', '.cart-collaterals .coupon .button.apply-coupon', function () {
            let couponCode = $('.cart-collaterals .coupon input[name*=coupon_code]').val();
            $('.woocommerce-cart-form #coupon_code').attr('value', couponCode);
            $('.woocommerce-cart-form .button[name*=apply_coupon]').trigger('click');
        });
    }

});