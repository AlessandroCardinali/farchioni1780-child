jQuery(document).ready(function($) {
    "use strict";

    // BugFix: Toggle mini cart non funziona più se aggiungi un prodotto al carrello fino al prossimo refresh.
    $('.elementor-menu-cart__toggle.elementor-button-wrapper').on('click', function() {
        if ($('body').hasClass('woocommerce-cart') || $('body').hasClass('woocommerce-checkout')) {
            // Do Nothing! We are in Cart or Checkout page
        } else {
            $('.elementor-menu-cart__container.elementor-lightbox').addClass('elementor-menu-cart--shown');
        }
    });

    /**
     * Apri il carrello quando un prodotto è stato aggiunto
     * La funzione è triggerata da Ajax add to cart for WooCommerce
     * https://wordpress.org/plugins/woo-ajax-add-to-cart/
     */
    $(document).on('added_to_cart', function() {
        $('.elementor-menu-cart__container.elementor-lightbox').addClass('elementor-menu-cart--shown');
    });


    /**
     * Aggiorna contatore prodotti nel carrello
     * aggiung un div al body dove get_fragments aggiornerà il numero di prodotti nel carrello
     * da li prendi ul numero e schiaffalo nella nostra icona del carrello
     */
    $('body').append('<div style="display:none !important; opacity:0 !important; font-size:0!important;" id="products-in-cart-hide"><span class="total-cart-item">(0)</span></div>');
    $(document).on('added_to_cart', function(e) {
        let productCount = $('.total-cart-item').text();
        productCount = productCount.replace('(', '');
        productCount = productCount.replace(')', '');
        $('#elementor-menu-cart__toggle_button .elementor-button-icon').attr('data-counter', productCount);
    });
});
