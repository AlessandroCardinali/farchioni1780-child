"use strict";

jQuery(document).ready(function ($) {
  "use strict";

  toggleSidebar();
  /**
   * Sidebar toggle
   */

  function toggleSidebar() {
    // Remove body class
    $('body').on('touchstart click', cancelDuplicates(function (e) {
      if ($(e.target).hasClass('sidebar-mobile-toggle-active')) {
        $('body').removeClass('sidebar-mobile-toggle-active');
      }
    })); // Add body class

    $('.open-mobile-sidebar').on('touchstart click', cancelDuplicates(function () {
      $('body').addClass('sidebar-mobile-toggle-active');
    }));
  }
  /**
   * Cancel duplicates functions within 10ms
   * https://stackoverflow.com/questions/25572070/javascript-touchend-versus-click-dilemma
   */


  function cancelDuplicates(fn, threshhold, scope) {
    if (typeof threshhold !== 'number') threshhold = 300;
    var last = 0;
    return function () {
      var now = +new Date();

      if (now >= last + threshhold) {
        last = now;
        fn.apply(scope || this, arguments);
      }
    };
  }
});
// jQuery(document).ready(function ($) {
//     "use strict";
//     let $shippingCountry = $('#shipping_country');
//     let $billingCountry = $('#billing_country');
//     $shippingCountry.attr('disabled', 'disabled');
//     // Shipping e Billing Country devono avere lo stesso valore
//     updateShippingCountrySelect($shippingCountry, $billingCountry);
//     /**
//      * Aggiorna il campo nazione di spedizione con il valore del campo nazione di fatturazione
//      * @param {*} $shippingCountry 
//      * @param {*} $billingCountry 
//      */
//     function updateShippingCountrySelect($shippingCountry, $billingCountry) {
//         $billingCountry.on('change', function(e) {
//             // Valore select utilizzato da WC
//             $shippingCountry.val($billingCountry.val());
//             // Valore frontend utilizzato dalla select dei paesi
//             $('#select2-shipping_country-container').text($('#shipping_country :selected').text());
//             $('body').trigger( 'update_checkout' );
//         });
//     }
// });
"use strict";
"use strict";

jQuery(document).ready(function ($) {
  "use strict";
  /**
   * Edit labels id
   */

  var privacyPolicyLabelId = 'afreg_additional_18481';
  var newsLetterLabelId = 'afreg_additional_18482';
  /**
   * Custom code - stop edit
   * Unless you know what you're doing
   */
  // Get current language

  var currentLang = $('body').attr('class');
  currentLang = currentLang.search('current_lang_it'); // -1 means the current_lang_it was not found in the body class

  if (currentLang === -1) {
    translateCheckbox();
  } else {
    addUrlToLabels();
  }
  /**
   * Traduci in inglese le due checkbox usate durante la creazione dell'account
   */


  function translateCheckbox() {
    $('label[for="' + privacyPolicyLabelId + '"]').html('I read and understood the <a href="https://store.farchioni1780.com/en/privacy-policy/">Privacy</a> and <a href="https://store.farchioni1780.com/en/cookies/">cookies</a> policy');
    $('label[for="' + newsLetterLabelId + '"]').html('I read and understood the <a href="https://store.farchioni1780.com/en/privacy-policy/">Commercial</a> policy');
  }
  /**
   * Converti le url in html nelle due label in italiano
   */


  function addUrlToLabels() {// TODO
  }
  
  
  
  $("li.wt-cli-manage-consent-link a").attr("href", "javascript:void(0)");
  $("li.wt-cli-manage-consent-link a").addClass('wt-cli-manage-consent-link');
  
});