<?php
/**
 * Template della pagina di attivazione di un utente
 * appena registratosi
 *
 * Vedi anche:
 * - https://github.com/spivurno/gfur-activate-template
 * - https://trello.com/c/6Lj6rJSC/
 */

defined( 'ABSPATH' ) || die();

define( 'WP_INSTALLING', true );

global $current_site;

// include GF User Registration functionality
require_once( gf_user_registration()->get_base_path() . '/includes/signups.php' );

GFUserSignups::prep_signups_functionality();

do_action( 'activate_header' );

function do_activate_header() {
	do_action( 'activate_wp_head' );
}

add_action( 'wp_head', 'do_activate_header' );

function wpmu_activate_stylesheet() {
	?>
	<style type="text/css">
		form {
			margin-top: 2em;
		}

		#submit, #key {
			width: 90%;
			font-size: 24px;
		}

		#language {
			margin-top: .5em;
		}

		.error {
			background: #f66;
		}
		#content {
			max-width: 900px;
			margin: auto;
			padding: 50px 0px 50px 0px;
		}
		p {
			font-size: 18px;
		}
	</style>
	<?php
}

add_action( 'wp_head', 'wpmu_activate_stylesheet' );

// If Elementor is installed, use its header instead of the default one
$isElementorActive = class_exists( 'ElementorPro\Modules\ThemeBuilder\Classes\Theme_Support' );
if ( $isElementorActive ) {
	$elementorThemeSupport = new ElementorPro\Modules\ThemeBuilder\Classes\Theme_Support();
	$elementorThemeSupport->get_header( '' );
}
else {
	get_header();
}

?>
<section class="elementor-section elementor-section-boxed">
	<div id="content" class="elementor-container elementor-column-gap-default">

		<?php if ( empty( $_GET['gfur_activation'] ) && empty( $_POST['gfur_activation'] ) ) { ?>

			<h2><?php _e( 'Activation Key Required' ) ?></h2>
			<form name="activateform" id="activateform" method="post" action="<?php echo network_site_url( '?page=gf_activation' ); ?>">
				<p>
					<label for="key"><?php _e( 'Activation Key:' ) ?></label>
					<br /><input type="text" name="key" id="key" value="" size="50" />
				</p>
				<p class="submit">
					<input id="submit" type="submit" name="Submit" class="submit" value="<?php esc_attr_e( 'Activate' ) ?>" />
				</p>
			</form>

		<?php } else {

			$key    = !empty( $_GET['gfur_activation'] ) ? $_GET['gfur_activation'] : $_POST['gfur_activation'];
			$result = GFUserSignups::activate_signup( $key );
			if ( is_wp_error( $result ) ) {
				if ( 'already_active' == $result->get_error_code() || 'blog_taken' == $result->get_error_code() ) {
					$signup = $result->get_error_data();
					?>
					<h2><?php _e( 'Your email address has already been confirmed', 'farchioni1780-child' ); ?></h2>
					<?php
					echo '<p class="lead-in poni">';
					if ( $signup->domain . $signup->path == '' ) {
						_e( '<strong>Your request is now being processed by our staff</strong>; in the event that you do not hear from us in the next 48 hours, please contact us using the Help section of this website.', 'farchioni1780-child' );
					} else {
						printf( __( 'Your site at <a href="%1$s">%2$s</a> is active. You may now log in to your site using your chosen username of &#8220;%3$s&#8221;. Please check your email inbox at %4$s for your login instructions. If you do not receive an email, please check your junk or spam folder. If you still do not receive an email within an hour, you can <a href="%5$s">reset your password</a>.' ), 'http://' . $signup->domain, $signup->domain, $signup->user_login, $signup->user_email, network_site_url( 'wp-login.php?action=lostpassword' ) );
					}
					echo '</p>';
				} else {
					?>
					<h2><?php _e( 'An error occurred during the activation, please contact us for support', 'farchioni1780-child' ); ?></h2>
					<?php
					echo '<p>' . $result->get_error_message() . '</p>';
				}
			} else {
				extract( $result );
				$url  = is_multisite() ? get_blogaddress_by_id( (int) $blog_id ) : home_url( '', 'http' );
				$user = new WP_User( (int) $user_id );
				?>
				<h2><?php _e( 'Thank you for confirming your email address', 'farchioni1780-child' ); ?></h2>

				<div id="signup-welcome">
					<p>
						<?php _e( '<strong>Your request is now being processed by our staff</strong>; in the event that you do not hear from us in the next 48 hours, please contact us using the Help section of this website.', 'farchioni1780-child' ); ?>
					</p>
				</div>
				<?php
			}
		}
		?>
	</div>
</section>
	<script type="text/javascript">
		var key_input = document.getElementById( 'key' );
		key_input && key_input.focus();
	</script>
<?php
// Use Elementor footer instead of the default one
if ( $isElementorActive ) {
	$elementorThemeSupport->get_footer( '' );
}
else {
	get_footer();
}
?>
